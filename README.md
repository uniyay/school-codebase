# uniyay-school-codebase

## Description
I use this repository to store all the programming scripts and codes that I have created (and am still creating) during my System Development Course.

## Cloning
To clone this repository, use: `git clone git@gitlab.com:uniyay/school-codebase.git`.

## Usage
You can use this project to study, fork, or contribute to make it better.

## Contributing
This project contains a wide variety of different technologies; therefore, there are no specified dependencies, and you must find them on your own. Most of the projects contained in this repository are very basic and do not require a complex dependency system. This means that you most likely won't need to install anything if you have already worked on similar projects before.

## Acknowledgment
Made by Vitor Góes.

## License
**GNU GPLv3** is used in this project.  
### This means that you can use this project for:
- Commercial uses;
- Distribution;
- Modification;
- Patent use;
- Private use.  
### As long as you follow these conditions:
- Disclose source;
- Include a copy of the license and copyright notice;
- Use the same license;
- State changes made to the licensed material.

## Project status
This project is currently active, but with less commits being made than usual.