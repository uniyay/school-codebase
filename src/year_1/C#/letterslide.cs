using System;
using System.Threading;

namespace App {
class Program {
	static void Main(string[] Args) {
		
		bool repeat = true, end = false;
		while(repeat) {
			Console.Clear();

			Console.Write("Digite a palavra que deseja animar: ");
			string quote = Console.ReadLine();
			Console.Clear();

			int qsize = quote.Length;
			double mult = 0.07, idelay = 10;

			window(quote);

			for (int x = 0; x < qsize; x++) {
				string qlett = quote.Substring(x,1);
				for(int y = 70; y > x; y--) {
					if(qlett == " " || qlett == "," || qlett == "." || qlett == "\"") {
						Console.SetCursorPosition(1+x,1);
						Console.Write(qlett);
						break;
					}

					if(y >= x+7) {idelay = 500*mult;}
					if(y >= x+12) {idelay = 190*mult;}
					if(y >= x+20) {idelay = 115*mult;}
					if(y >= x+30) {idelay = 85*mult;}
					if(y >= x+45) {idelay = 50*mult;}
					if(y >= x+60) {idelay = 25*mult;}

					int wdelay = Convert.ToInt32(idelay);
					Console.SetCursorPosition(y,1);
					Console.Write(qlett);
					Console.SetCursorPosition(y+1,1);
					Console.Write(" ");
					WriteAt("│", quote.Length+1, 1);
					Thread.Sleep(wdelay);
				}
			}
			Console.SetCursorPosition(0,3);
			string handler = "Deseja animar outra frase/palavra? (s/n)";
			while(repeat) {
				Console.Write($"{handler}\n");
				string answer = Console.ReadLine();
				if(answer == "s") {
					repeat = false;
					end = false;
				}
				else if(answer == "n") {
					repeat = false;
					end = true;
				}
				else {
					handler = "Opção não existente, responda apenas com \"s\" ou \"n\"";
				}
				Console.Clear();
			}
			if(!end) {
				repeat = true;
			}
		}
		Console.Clear();
	}
	static void window(string quote) {

		int y = 2, x = quote.Length+1;
		for (int i = 1; i <= x; i++) {
			WriteAt("─", i, 0);
			WriteAt("─", i, y);
		}
		for (int o = 1; o <= y; o++) {
			WriteAt("│", x, o);
			WriteAt("│", 0, o);
		}

		WriteAt("┌", 0, 0);
		WriteAt("┐", x, 0);
		WriteAt("└", 0, y);
		WriteAt("┘", x, y);
	}
	static void WriteAt(string word, int x, int y) {

    		Console.SetCursorPosition(x, y);
        	Console.Write(word);
        }
}
}