using System;

namespace Program {
class App {
	static void Main(string[] args) {
		Console.Clear();

		string handler = "Deseja ver as informações de outra frase? (s/n)", answer = "null";
		int[] result = {0,0,0,0};
		bool repeat = true, end = false;

		while (repeat) {
			Console.WriteLine("Escreva a frase que deseja ver informações sobre suas letras");
			string quote = Console.ReadLine();
			result = getLetter(quote);
			Console.Clear();
			while (repeat) {
				Console.Write($"Frase Escolhida: {quote}\n\nVogais: {result[0]}\nConsoantes: {result[1]}\nEspaços: {result[2]}\nTotal de letras: {result[3]}\n");
				Console.Write("──────────\n");
				Console.WriteLine(handler);
				answer = Console.ReadLine();
				if (answer == "s") {repeat = false; handler = "Deseja ver as informações de outra frase? (s/n)";}
				else if (answer == "n") {repeat = false; end = true;}
				else {handler = "Responda apenas com \"s\" ou \"n\"";}
				Console.Clear();
			}
			if (!end) {repeat = true;}
		}
	}
	static int[] getLetter(string quote) {
		int[] result = {0,0,0,0}; // vowels_amount = 0, consonants_amount = 0, spaces_amount = 0, total = 0;
		string quote_letter;
		string[] consonants = {
			"B","C","D",
			"F","G","H",
			"J","K","L",
			"M","N","P",
			"Q","R","S",
			"T","V","W",
			"X","Y","Z"
		};
		string[] vowels = {
			"A","Ã","Á","À","Â",
			"E","Ẽ","É","È","Ê",
			"I","Ĩ","Í","Ì","Î",
			"O","Õ","Ó","Ò","Ô",
			"U","Ũ","Ú","Ù","Û"
		};

		quote = quote.ToUpper();

		for (int x = 0; x < quote.Length; x++) {
			quote_letter = quote.Substring(x, 1);
			foreach (string consonant_letter in consonants) {
				if (quote_letter == consonant_letter) {result[1]++;}
			}
			foreach (string vowel_letter in vowels) {
				if (quote_letter == vowel_letter) {result[0]++;}
			}
			if (quote_letter == " ")  {result[2]++;}

			result[3] = result[0] + result[1];
		}

		return result;
	}
}
}