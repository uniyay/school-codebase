using System;

namespace Program {
class App {
	static void Main(string[] args) {
		
		Console.Clear();

		Console.Write("Escreva a Frase a ser traduzida: ");
		string quote = Console.ReadLine();
		quote = quote.ToUpper();
		
		Console.Clear();
			
		Console.WriteLine($"Frase Escolhida: {quote}");
		Console.WriteLine($"Frase Traduzida: {translate(quote)}");
	}
	static string translate(string quote) {
			
		Dictionary<string, string> zenit = new Dictionary<string, string>(){
			{"Ã", "Ĩ"}, {"Á", "Í"}, {"À", "Ì"}, {"Â", "Î"},
			{"Ẽ", "Õ"}, {"É", "Ó"}, {"È", "Ò"}, {"Ê", "Ô"},
			{"Z", "P"}, {"E", "O"}, {"N", "L"}, {"I", "A"}, {"T", "R"}
		};
		var polar = zenit.ToDictionary(x => x.Value, x => x.Key);
		
		string f_quote = "";
		int qSize = quote.Length;
		for (int i = 0; i < qSize; i++) {
			string qLetter = quote.Substring(i, 1);
			try {
				try{qLetter = zenit[qLetter];}
				catch{qLetter = polar[qLetter];}
			}
			catch {
				qLetter = qLetter;
			}
			f_quote += qLetter;
		}
		return f_quote;
	}
}
}