using System;

namespace App {
class Program {
	static void Main(string[] args) {

		string handler = "Insira a quantia de linhas que seram utilizadas (incluindo aquelas que ficaram vazias): ", answer;
		bool repeat = true, end = false;
		while(repeat) {
			try {
				Console.Clear();

				Console.Write(handler);
				int line_quant = int.Parse(Console.ReadLine());

				Console.Clear();

				string[] quote = new string[line_quant];
				for (int x = 0; x < line_quant; x++) {
					Console.Write($"Insira a {x+1}*frase: ");
					quote[x] = Console.ReadLine();
					Console.Clear();
				}

				window(quote);

				handler = "\nDeseja refazer a janela? (s/n)";
				while(repeat) {
					Console.WriteLine($"{handler}");
					answer = Console.ReadLine();
					if (answer == "s") {
						repeat = false;
					}
					else if (answer == "n") {
						repeat = false;
						end = true;
					}
					else {
						handler = "Responda apenas com \"s\" ou \"n\" (minúsculo)";
					}
					Console.Clear();
				}

				if(!end) {
					handler = "Insira a quantia de linhas que seram utilizadas (incluindo aquelas que ficaram vazias): ";
					repeat = true;
				}

				Console.Clear();
			}
			catch {
				handler = "Insira um valor númerico: ";
			}
		}
	}
	static void window(string[] quote) {
		
		int y = quote.Length+1, x = 0, p = 1;
		foreach (string qpart in quote) {
			WriteAt(qpart, 1, p);
			p++;
			if (qpart.Length >= x) {
				x = qpart.Length+1;
			}
		}
		for (int i = 1; i <= x; i++) {
			WriteAt("─", i, 0);
			WriteAt("─", i, y);
		}
		for (int o = 1; o <= y; o++) {
			WriteAt("│", x, o);
			WriteAt("│", 0, o);
		}
		
		WriteAt("┌", 0, 0);
		WriteAt("┐", x, 0);
		WriteAt("└", 0, y);
		WriteAt("┘", x, y);
	}
	static void WriteAt(string word, int x, int y) {
    		
    		Console.SetCursorPosition(x, y);
        	Console.Write(word);
        }
}
}
