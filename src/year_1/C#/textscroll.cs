using System;
using System.Threading;

namespace App {
class Program {
	static void Main (string[] args) {

		Console.Clear();

		string[] text = {" "};
		string tword = "a";
		bool repeat = true;

		Console.Write("Insira o texto desejado: ");
		text[0] = Console.ReadLine();
		text[0] = text[0].PadRight(70-text[0].Length,' ');

		while(repeat) {
			window(text);
			tword = text[0].Substring(0, 1);
			text[0] = text[0].Remove(0, 1);
			text[0] = text[0].Insert(text[0].Length,tword);
			Thread.Sleep(50);

		}

	}
	static void window(string[] quote) {

		int y = quote.Length+1, x = 0, p = 1;
		foreach (string qpart in quote) {
			WriteAt(qpart, 1, p);
			p++;
			if (qpart.Length > x) {
				x = qpart.Length+1;
			}
		}
		for (int i = 1; i <= x; i++) {
			WriteAt("─", i, 0);
			WriteAt("─", i, y);
		}
		for (int o = 1; o <= y; o++) {
			WriteAt("│", x, o);
			WriteAt("│", 0, o);
		}

		WriteAt("┌", 0, 0);
		WriteAt("┐", x, 0);
		WriteAt("└", 0, y);
		WriteAt("┘", x, y);
	}
	static void WriteAt(string word, int x, int y) {

    		Console.SetCursorPosition(x, y);
        	Console.Write(word);
        }
}
}