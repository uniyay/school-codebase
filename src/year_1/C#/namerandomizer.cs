// What does this code do?
// -----------------------
// This code will draw names using random generated numbers


using System;
using System.Linq;

namespace Program {
class App {
	static void Main(string[] args) {

		Console.Clear();

		// The code below and before "while" only creates the
		// required variables and arrays. A more detailed
		// explanation will be given after the "while"
		string handler = "Insira a quantia de nomes a serem sorteados", answer, name_gen;
		string[] names = new string[1];
		int name_amount = 0, name_gen_amount = 0, name_gen_num = 0;
		bool repeat = true, end = false;

		// Important!!! This is used to generate the random values
		Random rand = new Random();

		while(repeat) {
			//Will run the code below until an error occurs
			try {

				Console.Clear();

				//Display the handler, which its only purpose is to display an error if the inserted value is not accepted, or a message, if nothing happens
				//Also, the default message will ask the user how much people the code should randomly choose.
				Console.WriteLine(handler);
				Console.WriteLine("------------------------");
				Console.Write("Resposta: ");
				//User input goes here (Also, it is converted from string to int, since Console.ReadLine() only inputs strings)
				name_amount = int.Parse(Console.ReadLine());
				//Create an array to store names; Uses the top answer to define the maximum amount of names that can be inserted into it
				names = new string[name_amount];

				Console.Clear();

				//Ask the name of each person
				for(int x = 0; x < name_amount; x++) {
					Console.Write($"Insira o nome da pessoa N*{x+1}: ");
					names[x] = Console.ReadLine();
					Console.Clear();
				}
				//Writes the results (1st person chosen, 2nd person chosen...)
				for(int y = 0; y < name_amount; y++) {
					//Gets the amount of people from the array names
					name_gen_amount = names.Length;
					//Generates the random index to randomly choose a person
					name_gen_num = rand.Next(name_gen_amount);
					//Gets the randomly chosen name, using the random index (name_gen_num)
					name_gen = names[name_gen_num];
					//Remove the randomly chosen name from the "names" array so it cannot get chosen again
					names = names.Except(new string[]{name_gen}).ToArray();

					//Writes the output followed by a position (1st, 2nd, ...)
					Console.WriteLine("{0}* pessoa: {1}", y+1, name_gen);
				}

				//Changes the handler message so it can be used in another question
				handler = "Deseja refazer o sorteio? (s/n)";

				while(repeat) {
					//Asks the user if he/she wants to restart the code
					Console.WriteLine($"\n{handler}");
					Console.WriteLine("-------------------------------");
					Console.Write("Resposta: ");
					//Gets the answer (yes/no)
					answer = Console.ReadLine();

					//if the user answers "yes", it will end this loop
					if (answer == "s") {
						repeat = false;
					}
					//if the user answers "no", it will end this loop and program
					else if (answer == "n") {
						repeat = false;
						end = true;
					}
					//if the user answers neither yes nor no, it will display an error message (using the handler) saying: Option does not exist, use only yes or no
					else {
						Console.Clear();
						handler = "Opção não existente, use apenas s ou n";
					}
				}

				//if end is false (this means, the user does want to restart the program), restore the default values of "repeat", "handler" and "names"
				if (!end) {
					repeat = true;
					handler = "Insira a quantia de nomes desejados";
					Array.Clear(names, 0, names.Length);
				}

				Console.Clear();
			}
			//If a non-numerical value is inserted, set handler message to: Insert a numerical value [Should be used in conjunction with "try"]
			catch {

				handler = "Insira um valor númerico";
			}
		}
	}
}
}