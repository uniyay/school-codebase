using System;

namespace Program {
class App {
	static void Main(string[] args) {

		Console.Clear();

		Console.WriteLine("Quantas pessoas seram sorteadas?");
		int amount = int.Parse(Console.ReadLine());
		string[] names = new string[amount];
		for (int x = 0; x < amount; x ++) {
			Console.Clear();
			Console.Write("Nome da {0}* pessoa: ", x+1);
			names[x] = Console.ReadLine();
		}

		Console.Clear();

		var gen = RandGenerator(names);

		int index_num = 0, index_handler = 0, while_quant = 2;
		double index_amount = Math.Ceiling(amount/2.0);
		Console.ReadKey();
		for (int x = 0; x < index_amount; x++) {
			Console.Write("Grupo \"{0}\": ", x+1);
			if (amount % 2 == 1 && x == index_amount-1) {while_quant = 1;}
			while (index_handler < while_quant) {
				Console.Write("{0} ", gen[index_num]);
				index_num++;
				index_handler++;
			}
			Console.Write("\n");
			index_handler = 0;
		}

		Console.ReadKey();
	}
	static List<string> RandGenerator(string[] input) {

		int generated_number = 0;
		string generated_output = "null";
		List<string> output = new List<string>();
		Random rand = new Random();

		for (int x = 0; x < input.Length; x++) {
			do {
				generated_output = input[rand.Next(0, input.Length)];
			} while (output.Contains(generated_output));
			output.Add(generated_output);
		}

		return output;
	}
}
}