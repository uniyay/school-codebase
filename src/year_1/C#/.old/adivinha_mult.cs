﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var rnd = new Random();
            bool repeat = true;
            int qnt = 0, num = 0, answer = 0;
            string handler = "Selecione uma das opções";

            while(repeat)
            {
                try 
                {
                	Console.Clear();
                	
                	Console.WriteLine(handler);
                	Console.WriteLine("----------------------------");
                	Console.WriteLine("[1] 1 Jogador");
                	Console.WriteLine("[2] 2 Jogadores\n");
                	Console.WriteLine("[0] Sair");
                	
                	int usrip = int.Parse(Console.ReadLine());
                	
                	switch (usrip)
                	{
                		case 1:
                			num = rnd.Next(1,10);
                			game(num);
                			break;
                		case 2:
                			players();
                			break;
                		case 0:
                			repeat = false;
                			break;
                		default:
                			handler = "Opção Inválida";
                			break;
                	}
                }
                catch
                {
                	handler = "Valor Inválido";
                }
                
                
            }
            
            Console.Clear();
        }
        static int game(int num)
        {
            bool repeat = true;
            int qnt = 0;
            string handler = "Insira um número ";

            
            while(repeat)
            {
                try
                {
                    Console.Clear();

                    Console.WriteLine(handler);
                    Console.WriteLine("----------------------------");
                    int answer = int.Parse(Console.ReadLine());

                    if (answer < 1 || answer > 10)
                    {
                        handler = "Valor Inválido";
                    }
                    else if (answer < num)
                    {
                        qnt += 1;
                        handler = "Escolha um número maior";
                    }
                    else if (answer > num)
                    {
                        qnt += 1;
                        handler = "Escolha um número menor";
                    }
                    else
                    {
                        Console.Clear();
                        qnt += 1;
                        Console.WriteLine($"Correto! Tentativas: \"{qnt}\"; Número: \"{num}\"");
                        Console.WriteLine("----------------------------");
                        Console.ReadKey();
                        repeat = false;
                    }
                    
                }
                catch
                {
                    handler = "Valor Inválido";
                }
            }

            Console.Clear();
            return qnt;
        }
        static int number_select(int player)
        {
            bool repeat = true;
            int num=1;
            string handler = $"Insira o número ao qual o {player}° jogador deve descobrir";

            while (repeat)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine(handler);
                    Console.WriteLine("----------------------------");
                    num = int.Parse(Console.ReadLine());
                    if (num < 1 || num > 10)
                    {
                        handler = "Insira um valor entre 1 & 10";
                    }
                    else
                    {
                        repeat = false;
                    }
                }
                catch
                {
                    handler = "Valor Inválido";
                }
            }

            Console.Clear();
            return num;
        }
        static void players()
        {
            int player1, player2, p1num, p2num;
            bool repeat = true, end = false;
            string player1name, player2name, answer;

            while(repeat)
            {
                Console.Clear();

                p1num = number_select(1);
                p2num = number_select(2);

                Console.WriteLine("Insira o nome do 1º jogador");
                player1name = Console.ReadLine();
                player1 = game(p1num);

                Console.WriteLine("Insira o nome do 2º jogador");
                player2name = Console.ReadLine();
                player2 = game(p2num);

                Console.Clear();
                if(player1 < player2)
                {
                    Console.WriteLine($"O Jogador \"{player1name}\" é o vencedor! Com \"{player2-player1}\" tentativas a menos");
                }
                else if(player1 > player2)
                {
                    Console.WriteLine($"O Jogador \"{player2name}\" é o vencedor! Com \"{player1 - player2}\" tentativas a menos");
                }
                else
                {
                    Console.WriteLine($"Empate, ambos os jogadores obtiveram a mesma quantidade de tentativas! \"{player1}\"");
                }

                Console.WriteLine("----------------------------------------------------------------------------------------");
                while (repeat)
                {
                    Console.WriteLine("Deseja Continuar? (s/n)");
                    answer = Console.ReadLine();

                    if (answer=="s")
                    {
                        Console.Clear();
                        repeat = false;
                    }
                    else if (answer=="n")
                    {
                        repeat = false;
                        end = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Opção Inválida");
                        Console.WriteLine("----------------------------");
                    }
                }

                if (!end)
                {
                    repeat = true;
                }
                
            }
        }
    }
}