using System;

namespace workspace
{
class calc
{
static void Main (string[] args)
{
        Console.Clear ();

        bool repeat = true, end = false;
        string result = "null", header = "null", handler = "null";
        float var1 = 0, var2 = 0, opt = 99;

        while (repeat) {
                Console.WriteLine ("Iniciar programa? (s/n)\nResposta: ");
                string resp = Console.ReadLine();
                if (resp == "s" || resp == "S") {
                        while (repeat) {
                                Console.Clear ();
                                
                                if (header != "null") {Console.WriteLine ($"---------------------------\n{header}");}
                                Console.WriteLine ("---------------------------");
                                Console.WriteLine ("[1] Definir valores\n");
                                Console.WriteLine ("[2] Soma");
                                Console.WriteLine ("[3] Subtração");
                                Console.WriteLine ("[4] Multiplicação");
                                Console.WriteLine ("[5] Divisão\n");
                                Console.WriteLine ("[0] Sair");
                                Console.WriteLine ("---------------------------");
                                if (result != "null" && header != "null") {Console.WriteLine ($"{result}\n---------------------------");}
                                
                                if (handler != "null") {Console.WriteLine (handler);}
                                Console.WriteLine ("Opção: ");
                                try
                                {
                                	opt = float.Parse(Console.ReadLine());
                                }
                                catch
                                {
                                	handler = "!!! Opção Inválida\n---------------------------";
                                	continue;
                                }
                                
                                Console.Clear();                            
		                switch (opt)
		                {
		                        case 1:
		                        	while (repeat)
		                        	{
		                        	try
						{
						        Console.WriteLine ("Insira o valor #1: ");
						        var1 = float.Parse (Console.ReadLine());

						        Console.Clear ();

						        Console.WriteLine ("Insira o valor #2: ");
						        var2 = float.Parse (Console.ReadLine());

						        header = ($"V1: \"{var1}\"; V2: \"{var2}\"");
						        result = "null";
						        handler = "null";
						        repeat = false;
						}
						catch
						{
						        Console.Clear ();
						        Console.WriteLine ("Insira um valor númerico\n---------------------------");
						}
						}
						break;
					case 2:
						result = $"{var1} + {var2} = {var1+var2}";
						handler = "null";
						break;
					case 3:
						result = $"{var1} - {var2} = {var1-var2}";
						handler = "null";
						break;
					case 4:
						result = $"{var1} * {var2} = {Math.Round((var1*var2), 3)}";
						handler = "null";
						break;
					case 5:
						result = $"{var1} / {var2} = {Math.Round((var1/var2), 3)}";
						handler = "null";
						break;
					case 0:
						Console.Clear();
						repeat = false;
						end = true;
						break;
					default:
						handler = "!!! Opção Inválida\n---------------------------";
						break;
		                }
                        	if (!end)
                        	{
                        		repeat = true;
                        	}
                        }
                }
                else if (resp == "n" | resp == "N") {
                        Console.Clear ();
                        repeat = false;
                }
                else{
                        Console.Clear ();
                        Console.WriteLine ("Insira apenas \"s\" ou \"n\"\n---------------------------");
                }
        }
}
}
}
