﻿// Program made to demonstrate the use
// of functions on C# (C Sharp).

// By: Vitor Góes dos Santos - Etec Fernando Prestes (1DS)
using System;

namespace App
{
	class MyCode
	{
		static void Main(string[] args)
		{

			bool repeat = true, end = false;
			string handler = "Selecione uma das opções abaixo:";

			while (repeat)
			{ //Repeats the code until the user selects to end it.

				try
				{ //Runs the code.

					Console.Clear();

					Console.WriteLine(handler);
					Console.WriteLine("----------------------------------");
					Console.WriteLine("[1] Calculadora");
					Console.WriteLine("[2] Tabuada");
					Console.WriteLine("[3] Comparador");
					Console.WriteLine("[4] Vetores\n");
					Console.WriteLine("[0] Sair");

					int usrip = int.Parse(Console.ReadLine());

					switch (usrip)
					{
						case 1:
							calc();
							break;
						case 2:
							table();
							break;
						case 3:
							comp();
							break;
						case 4:
							vectors();
							break;
						case 0:
							Console.Clear();
							repeat = false;
							end = true;
							break;
						default:
							handler = "Valor Inválido";
							break;
					}
				}
				catch
				{ //Stops the Code if an Error is found and sends back a alert.
					handler = "Opção Inválida";
				}

				if (!end)
				{ //Checks if the user selected to end the program. If not, go back to start.
					repeat = true;
				}
			}
		}
		static void calc()
		{

			Console.Clear();

			bool repeat = true, end = false;

			while (repeat)
			{

				try
				{
					Console.WriteLine("Insira o #1 valor: ");
					float inp1 = float.Parse(Console.ReadLine());

					Console.Clear();

					Console.WriteLine("Insira o #2 valor: ");
					float inp2 = float.Parse(Console.ReadLine());

					Console.Clear();

					//Makes basic arithmetic.

					Console.WriteLine("Resultados: ");
					Console.WriteLine("--------------------");
					Console.WriteLine($"{inp1} + {inp2} = {inp1 + inp2}");
					Console.WriteLine($"{inp1} - {inp2} = {inp1 - inp2}");
					Console.WriteLine($"{inp1} * {inp2} = {Math.Round((inp1 * inp2), 3)}");
					Console.WriteLine($"{inp1} / {inp2} = {Math.Round((inp1 / inp2), 3)}");
					Console.WriteLine("--------------------");
					Console.WriteLine("Pressione qualquer tecla para sair.");
					Console.ReadKey();

					repeat = false;
					end = true;
				}
				catch
				{
					Console.Clear();
					Console.WriteLine("Valor Inválido\n--------------------");
				}

				if (!end)
				{
					repeat = true;
				}
			}
		}
		static void table()
		{

			Console.Clear();

			bool repeat = true, end = false;

			while (repeat)
			{

				try
				{
					Console.WriteLine("Insira o número a ser multiplicado: ");
					float inp1 = float.Parse(Console.ReadLine());

					Console.Clear();

					Console.WriteLine("Insira a quantia de vezes à multiplicar esse número: ");
					float inp2 = float.Parse(Console.ReadLine());

					Console.Clear();

					//Creates a multiplication table by using a simple "for" to count until it reaches the desired amount of multiplications.

					Console.WriteLine("Resultado:\n--------------------");
					for (int i = 0; i <= inp2; i++)
					{
						Console.WriteLine($"{inp1} * {i} = {Math.Round((inp1 * i), 3)}");
					}
					Console.WriteLine("Pressione qualquer tecla para sair.");
					Console.ReadKey();

					repeat = false;
					end = true;
				}
				catch
				{
					Console.Clear();
					Console.WriteLine("Valor Inválido\n--------------------");
				}

				if (!end)
				{
					repeat = true;
				}
			}
		}
		static void comp()
		{

			Console.Clear();

			bool repeat = true, end = false;
			float maior = 0, menor = 0;
			string tipo1 = "null", tipo2 = "null";

			while (repeat)
			{

				try
				{
					Console.WriteLine("Insira o #1 valor: ");
					float inp1 = float.Parse(Console.ReadLine());

					Console.Clear();

					Console.WriteLine("Insira o #2 valor: ");
					float inp2 = float.Parse(Console.ReadLine());

					Console.Clear();

					//Detects the biggest number and if it is an odd or pair (Both the 2 numbers).

					if (inp2 > inp1)
					{
						maior = inp2;
						menor = inp1;
					}
					else
					{
						maior = inp1;
						menor = inp2;
					}

					if ((inp1 % 2) == 0)
					{
						tipo1 = "Par;";
					}
					else
					{
						tipo1 = "Impar;";
					}
					if ((inp2 % 2) == 0)
					{
						tipo2 = "Par;";
					}
					else
					{
						tipo2 = "Impar;";
					}

					//Prints the Result.

					Console.WriteLine($"Valor1: \"{inp1}\"; Valor2: \"{inp2}\"\n--------------------");
					Console.WriteLine($"Maior: {maior}");
					Console.WriteLine($"Menor: {menor}");
					Console.WriteLine($"V#1 \"{inp1}\" = {tipo1}");
					Console.WriteLine($"V#2 \"{inp2}\" = {tipo2}");
					Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
					Console.ReadKey();

					repeat = false;
					end = true;
				}
				catch
				{
					Console.Clear();
					Console.WriteLine("Valor Inválido\n--------------------");
				}

				if (!end)
				{
					repeat = true;
				}
			}
		}
		static void calendar()
        {
			Console.Clear();

			bool repeat = true, end = false;
			string[] Mes = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
			string[] Dia = { "Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado" };
			string handler = "Dia&Mês por Extenso";
			string result = "null";

			while (repeat)
            {
				try
                {
					Console.WriteLine(handler);
					Console.WriteLine("--------------------");
					Console.WriteLine("Consultar: [1] Mês; [2] Dia\n");
					int inp1 = int.Parse(Console.ReadLine());

					if (inp1 != 1 || inp1 != 2)
                    {
						Console.Clear();

						handler = "Valor Inválido";
						continue;
					}

					Console.Clear();

					Console.WriteLine("Insira o valor do Dia/Mês desejado: ");
					int inp2 = int.Parse(Console.ReadLine());

					Console.Clear();

					if (inp1 == 1 && inp2 <= 12 && inp2 > 0)
                    {
						result = Mes[inp2 - 1];
                    }
					else if (inp1 == 2 && inp2 <= 7 && inp2 > 0)
					{
						result = Mes[inp2 - 1];
					}
					else
                    {
						Console.Clear();

						handler = "Valor Inválido";
						continue;
                    }

					Console.Clear();

					Console.WriteLine($"Valor \"{inp2}\" = {result}");
					Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
					Console.ReadKey();

					repeat = false;
					end = true;
				}
				catch
                {
					handler = "Opção Inválida";
				}

				if (!end)
                {
					repeat = true;
                }
            }
		}
		static void vectors()
        {
			bool repeat = true, end = false;
			string handler = "Selecione uma das opções abaixo:";

			while (repeat)
			{

				try
				{

					Console.Clear();

					Console.WriteLine(handler);
					Console.WriteLine("----------------------------------");
					Console.WriteLine("[1] Dia&Mes");
					Console.WriteLine("[2] GeoConversor");
					Console.WriteLine("[3] Extenso\n");
					Console.WriteLine("[0] Sair");

					int usrip = int.Parse(Console.ReadLine());

					switch (usrip)
					{
						case 1:
							calendar();
							break;
						case 2:
							table();
							break;
						case 3:
							comp();
							break;
						case 0:
							Console.Clear();
							repeat = false;
							end = true;
							break;
						default:
							handler = "Valor Inválido";
							break;
					}
				}
				catch
				{
					handler = "Opção Inválida";
				}

				if (!end)
				{
					repeat = true;
				}
			}
		}
	}
}