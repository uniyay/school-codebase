using System;
using System.Threading;

namespace App {
class MyCode {
static void Main(string[] args) {
	
	Console.Clear();
	
	var rand = new Random();
	
	Console.WriteLine("Escolha a palavra da qual deseje usar como ScreenSaver:");
	string word = Console.ReadLine();
	
	Console.Clear();
	while (true) {
		Console.SetCursorPosition(rand.Next(1,70), rand.Next(1,70));
		Console.ForegroundColor=(ConsoleColor)rand.Next(1,16);
		Console.Write(word);
		Thread.Sleep(1000);
	}
	
}
}
}