// App made to demonstrate the use
// of functions on C# (C Sharp).

// By: Vitor Góes dos Santos - Etec Fernando Prestes (1DS)

using System;

namespace App {
class MyCode {
static void Main(string[] args) {
	
	bool repeat = true, end = false;
	string handler = "Selecione uma das opções abaixo:";
	
	while (repeat) { //Repeats the code until the user selects to end it.
		
		try { //Runs the code.
			
			Console.Clear();
			
			Console.WriteLine(handler);
			Console.WriteLine("----------------------------------");
			Console.WriteLine("[1] Calculadora");
			Console.WriteLine("[2] Tabuada");
			Console.WriteLine("[3] Comparador");
			Console.WriteLine("[4] Vetores\n");
			Console.WriteLine("[0] Sair");
			
			int usrip = int.Parse(Console.ReadLine());
			
			switch (usrip) {
				case 1:
					calc();
					break;
				case 2:
					table();
					break;
				case 3:
					comp();
					break;
				case 4:
					vector();
					break;
				case 0:
					Console.Clear();
					repeat = false;
					end = true;
					break;
				default:
					handler = "Valor Inválido";
					break;
			}
		}
		catch { //Stops the Code if an Error is found and sends back a alert.
			handler = "Opção Inválida";
		}
		
		if (!end) { //Checks if the user selected to end the program. If not, go back to start.
			repeat = true;
		}
	}
}
static void vector() {
	bool repeat = true, end = false;
	string handler = "Selecione uma das opções abaixo:";
	
	while (repeat) {
		
		try {
			
			Console.Clear();
			
			Console.WriteLine(handler);
			Console.WriteLine("----------------------------------");
			Console.WriteLine("[1] Dia&Mês");
			Console.WriteLine("[2] Estados");
			Console.WriteLine("[3] Extenso\n");
			Console.WriteLine("[0] Sair");
			
			int usrip = int.Parse(Console.ReadLine());
			
			switch (usrip) {
				case 1:
					calendar();
					break;
				case 2:
					states();
					break;
				case 3:
					num();
					break;
				case 0:
					Console.Clear();
					repeat = false;
					end = true;
					break;
				default:
					handler = "Valor Inválido";
					break;
			}
		}
		catch {
			handler = "Opção Inválida";
		}
		
		if (!end) {
			repeat = true;
		}
	}
}

//Functions

static void calc() {
	
	Console.Clear();	
	
	bool repeat = true, end = false;
	
	while (repeat) {
		
		try {
			Console.WriteLine("Insira o #1 valor: ");
			float inp1 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			Console.WriteLine("Insira o #2 valor: ");
			float inp2 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			//Makes basic arithmetic.
			
			Console.WriteLine("Resultados: ");
			Console.WriteLine("--------------------");
			Console.WriteLine($"{inp1} + {inp2} = {inp1+inp2}");
			Console.WriteLine($"{inp1} - {inp2} = {inp1-inp2}");
			Console.WriteLine($"{inp1} * {inp2} = {Math.Round((inp1*inp2), 3)}");
			Console.WriteLine($"{inp1} / {inp2} = {Math.Round((inp1/inp2), 3)}");
			Console.WriteLine("--------------------");
			Console.WriteLine("Pressione qualquer tecla para sair.");
			Console.ReadKey();
			
			repeat = qcontinue();
			end = !repeat;
		}
		catch {
			Console.Clear();
			Console.WriteLine("Valor Inválido\n--------------------");
		}
		
		if (!end) {
			repeat = true;
		}
	}
}
static void table() {
	
	Console.Clear();
	
	bool repeat = true, end = false;
	
	while (repeat) {
		
		try {
			Console.WriteLine("Insira o número a ser multiplicado: ");
			float inp1 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			Console.WriteLine("Insira a quantia de vezes à multiplicar esse número: ");
			float inp2 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			//Creates a multiplication table by using a simple "for" to count until it reaches the desired amount of multiplications.
			
			Console.WriteLine("Resultado:\n--------------------");
			for (int i=1;i<=inp2;i++) {
				Console.WriteLine($"{inp1} * {i} = {Math.Round((inp1*i), 3)}");
			}
			Console.WriteLine("Pressione qualquer tecla para sair.");
			Console.ReadKey();
			
			repeat = qcontinue();
			end = !repeat;
		}
		catch {
			Console.Clear();
			Console.WriteLine("Valor Inválido\n--------------------");
		}
		
		if (!end) {
			repeat = true;
		}
	}
}
static void comp() {

	Console.Clear();
	
	bool repeat = true, end = false;
	float maior = 0, menor = 0;
	string tipo1 = "null", tipo2 = "null";
	
	while (repeat) {
		
		try {
			Console.WriteLine("Insira o #1 valor: ");
			float inp1 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			Console.WriteLine("Insira o #2 valor: ");
			float inp2 = float.Parse(Console.ReadLine());
			
			Console.Clear();
			
			//Detects the biggest number and if it is an odd or pair (Both the 2 numbers).
			
			if (inp2 > inp1) {
				maior = inp2;
				menor = inp1;
			}
			else {
				maior = inp1;
				menor = inp2;
			}
			
			if ((inp1%2) == 0) {
				tipo1 = "Par;";
			}
			else {
				tipo1 = "Impar;";
			}
			if ((inp2%2) == 0) {
				tipo2 = "Par;";
			}
			else {
				tipo2 = "Impar;";
			}
			
			//Prints the Result.
			
			Console.WriteLine($"Valor1: \"{inp1}\"; Valor2: \"{inp2}\"\n--------------------");
			Console.WriteLine($"Maior: {maior}");
			Console.WriteLine($"Menor: {menor}");
			Console.WriteLine($"V#1 \"{inp1}\" = {tipo1}");
			Console.WriteLine($"V#2 \"{inp2}\" = {tipo2}");
			Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
			Console.ReadKey();
			
			repeat = qcontinue();
			end = !repeat;
		}
		catch {
			Console.Clear();
			Console.WriteLine("Valor Inválido\n--------------------");
		}
		
		if (!end) {
			repeat = true;
		}
	}
}
static void calendar() {

	Console.Clear();
	
	bool repeat = true, end = false;
	string[] mes = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
	string[] dia = { "Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado" };
	string handler = "Dia&Mês por Extenso", result = "null";
	
	while (repeat) {
		
		try {
			Console.WriteLine(handler);
			Console.WriteLine("--------------------");
			Console.WriteLine("Consultar: [1] Mês; [2] Dia\n");
			int inp1 = int.Parse(Console.ReadLine());

			if (inp1 < 1 || inp1 > 2) {
				Console.Clear();

				handler = "Valor Inválido";
				continue;
			}

			Console.Clear();

			Console.WriteLine("Insira o valor do Dia/Mês desejado: ");
			int inp2 = int.Parse(Console.ReadLine());

			Console.Clear();

			if (inp1 == 1 && inp2 <= 12 && inp2 > 0) {
				result = mes[inp2 - 1];
			}
			else if (inp1 == 2 && inp2 <= 7 && inp2 > 0) {
				result = dia[inp2 - 1];
			}
			else {
				Console.Clear();

				handler = "Valor Inválido";
				continue;
			}

			Console.Clear();

			Console.WriteLine($"Valor \"{inp2}\" = {result}");
			Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
			Console.ReadKey();

			repeat = qcontinue();
			end = !repeat;
			
		}
		catch {
			Console.Clear();
			handler = "Opção Inválida";
		}
		
		if (!end) {
			repeat = true;
		}
	}
}
static void states() {
	
	Console.Clear();
	
	bool repeat = true, end = false;
	string[,] table = new string[27, 3];
	string result = "null", handler = "Escreva a sigla, capital ou nome de um Estado";
	
	table[0, 0] = "ACRE";                 table[0, 1] = "AC";   table[0, 2] = "RIO BRANCO";
	table[1, 0] = "ALAGOAS";              table[1, 1] = "AL";   table[1, 2] = "MACEIÓ";
	table[2, 0] = "AMAPÁ";                table[2, 1] = "AP";   table[2, 2] = "MACAPÁ";
	table[3, 0] = "AMAZONAS";             table[3, 1] = "AM";   table[3, 2] = "MANAUS";
	table[4, 0] = "BAHIA";                table[4, 1] = "BA";   table[4, 2] = "SALVADOR";
	table[5, 0] = "CEARÁ";                table[5, 1] = "CE";   table[5, 2] = "FORTALEZA";
	table[6, 0] = "ESPÍRITO SANTO";       table[6, 1] = "ES";   table[6, 2] = "VITÓRIA";
	table[7, 0] = "GOIÁS";                table[7, 1] = "GO";   table[7, 2] = "GOIÂNIA";
	table[8, 0] = "MARANHÃO";             table[8, 1] = "MA";   table[8, 2] = "SÃO LUÍS";
	table[9, 0] = "MATO GROSSO";          table[9, 1] = "MT";   table[9, 2] = "CUIABÁ";
	table[10, 0] = "MATO GROSSO DO SUL";  table[10, 1] = "MS";  table[10, 2] = "CAMPO GRANDE";
	table[11, 0] = "MINAS GERAIS";        table[11, 1] = "MG";  table[11, 2] = "BELO HORIZONTE";
	table[12, 0] = "PARÁ";                table[12, 1] = "PA";  table[12, 2] = "BELÉM";
	table[13, 0] = "PARAÍBA";             table[13, 1] = "PB";  table[13, 2] = "JOÃO PESSOA";
	table[14, 0] = "PARANÁ";              table[14, 1] = "PR";  table[14, 2] = "CURITIBA";
	table[15, 0] = "PERNAMBUCO";          table[15, 1] = "PE";  table[15, 2] = "RECIFE";
	table[16, 0] = "PIAUÍ";               table[16, 1] = "PI";  table[16, 2] = "TERESINA";
	table[17, 0] = "RIO DE JANEIRO";      table[17, 1] = "RJ";  table[17, 2] = "RIO DE JANEIRO";
	table[18, 0] = "RIO GRANDE DO NORTE"; table[18, 1] = "RN";  table[18, 2] = "NATAL";
	table[19, 0] = "RIO GRANDE DO SUL";   table[19, 1] = "RS";  table[19, 2] = "PORTO ALEGRE";
	table[20, 0] = "RONDÔNIA";            table[20, 1] = "RO";  table[20, 2] = "PORTO VELHO";
	table[21, 0] = "RORAIMA";             table[21, 1] = "RR";  table[21, 2] = "BOA VISTA";
	table[22, 0] = "SANTA CATARINA";      table[22, 1] = "SC";  table[22, 2] = "FLORIANÓPOLIS";
	table[23, 0] = "SÃO PAULO";           table[23, 1] = "SP";  table[23, 2] = "SÃO PAULO";
	table[24, 0] = "SERGIPE";             table[24, 1] = "SE";  table[24, 2] = "ARACAJU";
	table[25, 0] = "TOCANTINS";           table[25, 1] = "TO";  table[25, 2] = "PALMAS";
	table[26, 0] = "DISTRITO FEDERAL";    table[26, 1] = "DF";  table[26, 2] = "BRASÍLIA";
	
	while (repeat) {
	
		Console.WriteLine(handler);
		Console.WriteLine("--------------------");
		string usrip = Console.ReadLine();
		usrip = usrip.ToUpper();
		
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 27; y++) {
				if (table[y, x] == usrip) {
					result = $"Estado: {table[y, 0]}; Sigla: {table[y, 1]}; Capital: {table[y, 2]}";
				}
			}
		}
			
		if (result == "null") {
			Console.Clear();
			handler = "Estado não encontrado";
			continue;
		}
			
		Console.Clear();
			
		Console.WriteLine(result);
		Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
		Console.ReadKey();
			
		repeat = qcontinue();
		end = !repeat;
		
		if (!end) {
			repeat = true;
		}
	}
}
static void num() {
	Console.Clear();
	
	bool repeat = true, end = false;
	string handler = "Insira um número para ser convertido";
	string[] unit = {"ZERO","UM","DOIS","TRÊS","QUATRO","CINCO","SEIS","SETE","OITO","NOVE","DEZ","ONZE","DOZE","TREZE","QUATORZE","QUINZE","DEZESSEIS","DEZESSETE","DEZOITO","DEZENOVE"};
	string[] ten = {"ZERO","DEZ","VINTE","TRINTA","QUARENTA","CINQUENTA","SESSENTA","SETENTA","OITENTA","NOVENTA"};
	string[] hundred = {"ZERO","CENTO","DUZENTOS","TREZENTOS","QUATROCENTOS","QUINHENTOS","SEISCENTOS","SETECENTOS","OITOCENTOS","NOVECENTOS"};
	int r = 0, m = 0, l = 0;
	
	
	while (repeat) {
		
		try {
			Console.WriteLine(handler);
			Console.WriteLine("--------------------");
			int usrip = int.Parse(Console.ReadLine());
			
			Console.Clear();
			
			if (usrip < 0) {
				Console.Write("MENOS ");
			}
			
			usrip = Math.Abs(usrip);
			
			if (usrip < 20) {
				Console.Write(unit[usrip]);
			}
			else if (usrip < 100) {
				l = usrip/10;
				r = usrip%10;
				
				Console.Write(ten[l]);
				if (r != 0) {Console.Write($" e {unit[r]}");}
			}
			else if (usrip == 100) {Console.Write("CEM");}
			else if (usrip < 1000) {
				l = usrip/100;
				m = (usrip - l*100)/10;
				r = usrip-(l*100 + m*10);
				
				Console.Write(hundred[l]);
				if (m != 0) {Console.Write($" e {ten[m]}");}
				if (r != 0) {Console.Write($" e {unit[r]}");}
			}
			else if (usrip == 1000) {Console.Write("MIL");}
			else {Console.Write("Insira um valor até 1000");}
			
			Console.Write("\n");
			Console.WriteLine($"--------------------\nPressione qualquer tecla para sair.");
			Console.ReadKey();
			repeat = qcontinue();
			end = !repeat;
		}
		catch {
			Console.Clear();
			handler = "Valor Inválido";
			
		}
		
		if (!end) {
			repeat = true;
		}
	}
}
static bool qcontinue() {
	Console.Clear();
	
	bool repeat = true;
	
	while (repeat) {
		
		Console.WriteLine("Deseja fechar o código? (s/n)");
		string result = Console.ReadLine();
		
		Console.Clear();
		if (result == "s") {
			return false;
		}
		else if (result == "n") {
			return true;
		}
		else {
			Console.WriteLine("Opção Inválida\n----------------------------------");
		}
	}
	return true;
}
}
}
