using System;

namespace App {
class Program {
	static void Main(string[] args) {
		
		bool repeat = true;
		string handler = "Insira qualquer valor entre 1 & 10", answer = "Digito: ";
		int qnt = 0;
		
		Random rnd = new Random();
		int num = rnd.Next(1,10);
		
		while (repeat) {
			
			try {
				
				Console.Clear();
				
				Console.WriteLine(handler);
				Console.WriteLine("--------------------");
				if(answer=="Digito: "){Console.WriteLine(answer);} else{Console.WriteLine(answer);Console.ReadKey();repeat=false;break;}
				int guess = int.Parse(Console.ReadLine());
				
				if (guess < 1 || guess > 10) {handler = "Insira um valor entre 1 & 10";}
				else if (guess != num) {
				
					qnt += 1;
					if (num > guess) {handler="O número desejado é maior";}
					else {handler="O número desejado é menor";}
				}
				else {
				
					handler = $"Correto! Tentativas: \"{qnt}\"; Número: \"{num}\"";
					answer = "Pressione qualquer tecla para sair";
				}
			}
			catch {
				
				handler = "Insira um valor númerico";
			}
		}
	}
}
}