using System;

namespace App {
class Program {
	static void Main(string[] args) {
		
		Console.Clear();
		
		string quote = Console.ReadLine();
		int qsize = quote.Length;
		
		Console.Clear();
		
		Console.WriteLine($"Original: \"{quote}\"");
		Console.Write($"Reverso: \"");
		for (int x = qsize-1; x >= 0; x--) {
			string qword = quote.Substring(x,1);
			Console.Write(qword);
		}
		Console.Write("\"");
	}
}
}
