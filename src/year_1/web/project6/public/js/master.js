systemTheme();

function code() {
    
    var num=0;
	  
    var value1 = document.form_area.table_1.value;
	var value2 = document.form_area.table_2.value;
	var value3 = document.form_area.table_3.value;
	var value4 = document.form_area.table_4.value;
	var value5 = document.form_area.table_5.value;

	if(value1 == "true") {
        num = num+1;
    }
	if(value2 == "true") {
        num = num+2;
    }
    if(value3 == "true") {
        num = num+4;
    }
    if(value4 == "true") {
        num = num+8;
    }
    if(value5 == "true") {
        num = num+16;
    }
    if(num < 1) {
        num = "Hmm";
        document.getElementById('modal_body').innerHTML = 'Que pena, parece que você escolheu um número menor que 1 ou maior que 31, tente escolher algum outro número.'
    }
    else {
        document.getElementById('modal_body').innerHTML = 'Espero que tenha acertado! Tenho quase certeza que este é o número pensado!'
    }

    document.getElementById('modal_header').innerHTML = num;
    toggleModal();
}

function code2() {
    
    var images = [
    'content/image1.jpg', 'content/image2.jpg', 'content/image3.jpg',
    'content/image4.jpg', 'content/image5.jpg', 'content/image6.jpg',
    'content/image7.jpg', 'content/image8.jpg', 'content/image9.jpg',
    'content/image10.jpg', 'content/image11.jpg', 'content/image12.jpg',
    'content/image13.jpg', 'content/image14.jpg', 'content/image15.jpg'
    ];
    var names = [
    'Álvaro Morata', 'Ansu Fati', 'Aymeric Laporte',
    'César Azpilicueta', 'Dani Olmo', 'Eric García',
    'Gavi', 'Koke', 'Marcos Llorente',
    'Pablo Sarabia', 'Pau Torres', 'Pedri',
    'Sergio Busquets', 'Rodri', 'Robert Sánchez'
    ];

    var num=0;
	  
    var value1 = document.form_area.table_1.value;
	var value2 = document.form_area.table_2.value;
	var value3 = document.form_area.table_3.value;
	var value4 = document.form_area.table_4.value;

	if(value1 == "true") {
        num = num+1;
    }
	if(value2 == "true") {
        num = num+2;
    }
    if(value3 == "true") {
        num = num+8;
    }
    if(value4 == "true") {
        num = num+4;
    }
    if(num < 1) {
        document.getElementById('modal_img').classList.replace('block', 'hidden');
        document.getElementById('modal_body').classList.replace('hidden', 'block');
    }
    else {
        document.getElementById('modal_img').classList.replace('hidden', 'block');
        document.getElementById('modal_img').src = images[num-1];
        document.getElementById('modal_img').alt = names[num-1];
        document.getElementById('modal_body').classList.replace('block', 'hidden');
    }

    toggleModal();
}

function toggleTheme() {
    if (localStorage.theme === 'light') {
        localStorage.theme = 'dark';
        document.documentElement.classList.add('dark');
        document.getElementById('themeIcon').classList.replace('fa-moon', 'fa-sun');
        document.getElementById('themeIconMobile').classList.replace('fa-moon', 'fa-sun');
    }
    else {
        localStorage.theme = 'light';
        document.documentElement.classList.remove('dark');
        document.getElementById('themeIcon').classList.replace('fa-sun', 'fa-moon');
        document.getElementById('themeIconMobile').classList.replace('fa-sun', 'fa-moon');
    }
}

function toggleMenu() {
    var menu = document.getElementById('nav-mobile-menu');
    isHidden = menu.classList.contains('hidden');
    
    if (isHidden) {
        menu.classList.replace('hidden', 'block');
    }
    else {
        menu.classList.replace('block', 'hidden');
    }
}

function toggleModal() {
    if (document.getElementById('modal').classList.contains('hidden')) {
        document.getElementById('modal').classList.replace('hidden', 'flex');
    }
    else {
        document.getElementById('modal').classList.replace('flex', 'hidden');
    }
}

function systemTheme() {
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark')
        document.getElementById('themeIcon').classList.replace('fa-moon', 'fa-sun');
    }
    else {
        document.documentElement.classList.remove('dark');
        document.getElementById('themeIcon').classList.replace('fa-sun', 'fa-moon');
        
    }
}

function slideTo(id) {
    var element = document.getElementById(id);
    window.scrollTo({
        top: element.offsetTop,
        left: 0,
        behavior: 'smooth'
      });
      
}
