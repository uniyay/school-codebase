if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	./twlinux -i .styles.css -o ../../public/css/tailwind.css --watch --minify
else
	./twindows -i .styles.css -o  ../../public/css/tailwind.css --watch
fi
