/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
    '../../public/index.html',
    '../../public/pag_images.html',
    '../../public/pag_numbers.html'
    ],
theme: {
    extend: {},
},
plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
        require('@tailwindcss/line-clamp'),
        require('@tailwindcss/aspect-ratio'),
    ],
}
