/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    '../../public/index.html',
    '../../public/pag1_doutor.html',
    '../../public/pag2_tardis.html',
    '../../public/pag3_aliados.html',
    '../../public/pag4_inimigos.html',
    '../../public/pag5_outros.html',
    '../../public/js/master.js'
    
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
