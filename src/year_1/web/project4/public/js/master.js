function hidemenu() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    }
    else {
        x.style.display = "block";
    }
}

function transition_in() {
    var x = document.getElementById("workspace");
    x.style.opacity = "100"
}

function transition_out() {
    var x = document.getElementById("workspace");
    x.style.opacity = "0"
}