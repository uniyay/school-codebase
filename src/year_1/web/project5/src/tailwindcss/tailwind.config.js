/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    '../../public/index.html',
    '../../public/js/master.js'
    
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
