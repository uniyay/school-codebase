function code() {
    var candidates = ["Jair Bolsonaro","Luiz Inácio (Lula)","Felipe D'Avila","Soraya Thronicke","Simone Tebet","Ciro Gomes"]
    var candidates_images = ['content/bolsonaro_square.jpg','content/lula_square.jpg','content/felipe_square.jpg','content/soraya_square.jpg','content/simone_square.jpg','content/ciro_square.jpg']
    var candidates_votes = [0,0,0,0,0,0]; // 6 elements
    var white_null = [0,0];

    var plp_amount = document.getElementById('form_input').value;

    if (plp_amount < 2) {
        document.getElementById('form_input').classList.replace('bg-zinc-100', 'bg-red-300');
        document.getElementById('form_input').classList.replace('dark:bg-zinc-900', 'dark:bg-red-500');
        document.getElementById('form_input').classList.add('drop-shadow-lg');
        return false;
    }
    else {
        document.getElementById('form_input').classList.replace('bg-red-300', 'bg-zinc-100');
        document.getElementById('form_input').classList.remove('drop-shadow-lg');
    }

    for(x = 0; x < plp_amount; x++) {
        value = prompt('Digite o número do candidato\n\n[22] Bolsonaro\n[13] Lula\n[30] Felipe\n[44] Soraya\n[12] Simone\n[15] Ciro\n\n[0] Branco\n[Vazio] Nulo\n ');
        if (value == 22) { //Bolsonaro
            candidates_votes[0]++;
        }
        else if (value == 13) { //Lula
            candidates_votes[1]++;
        }
        else if (value == 30) { //Felipe
            candidates_votes[2]++;
        }
        else if (value == 44) { //Soraya
            candidates_votes[3]++;
        }
        else if (value == 12) { //Simone
            candidates_votes[4]++;
        }
        else if (value == 15) { //Ciro
            candidates_votes[5]++;
        }
        else if (value == 0) { //Branco
            white_null[0]++;
        }
        else { //Nulo
            white_null[1]++;
        }
    }

    result = `Não houve vencedor`;
    candidate_winner_name = ``;
    candidate_winner_amount = 0;
    candidate_winner_index = 6;
    if_candidates_draw = [];

    for(y = 0; y < 8; y++) {
        if (candidates_votes[y] == candidate_winner_amount && candidates_votes[y] != 0) {
            result = `Empate:`;
            candidate_winner_index = 6;
            if_candidates_draw.push(y);
        }
        else if (candidates_votes[y] > candidate_winner_amount) {
            result = `Vencedor:`;
            candidate_winner_name = candidates[y];
            candidate_winner_amount = candidates_votes[y];
            candidate_winner_index = y;
        }
    }

    if(if_candidates_draw.length > 0 && candidates_votes[if_candidates_draw[0]] == candidate_winner_amount) {
        candidates_winner_name = `Empate:`;
        for (z = 0; z < if_candidates_draw.length; z++) {
            candidate_winner_name += ` | ${candidates[if_candidates_draw[z]]}`;
        }
    }

    if(result == `Empate:` || result == `Não houve vencedor`) {
        toggleButton();
    }
    
    toggleModal();

    document.getElementById('title').innerHTML = `${result} ${candidate_winner_name}`;
    document.getElementById('image-box').src = candidates_images[candidate_winner_index];
    document.getElementById('bolsonaro').innerHTML = candidates_votes[0];
    document.getElementById('lula').innerHTML = candidates_votes[1];
    document.getElementById('felipe').innerHTML = candidates_votes[2];
    document.getElementById('soraya').innerHTML = candidates_votes[3];
    document.getElementById('simone').innerHTML = candidates_votes[4];
    document.getElementById('ciro').innerHTML = candidates_votes[5];
    document.getElementById('brancos').innerHTML = white_null[0];
    document.getElementById('nulos').innerHTML = white_null[1];
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// slides Start

var slide_images = ['url(content/bolsonaro.jpg)','url(content/lula.jpg)','url(content/felipe.jpg)','url(content/soraya.jpg)','url(content/simone.jpg)','url(content/ciro.jpg)'];
var slide_texts = ['Bolsonaro - 22','Lula - 13','Felipe - 30','Soraya - 44','Simone - 12','Ciro - 15'];
slide_pos = 4;

slideNext()

async function slideNext() {
    slide_pos++;
    if (slide_pos > 5) {
        slide_pos = 0;
    }
    
    document.getElementById('slide_image').classList.replace('opacity-10', 'opacity-100');
    document.getElementById('slide_text_box').classList.replace('opacity-0', 'opacity-100');
    document.getElementById('slide_image').style.backgroundImage = slide_images[slide_pos];
    document.getElementById('slide_text').innerHTML = slide_texts[slide_pos];
    await sleep(7000);
    document.getElementById('slide_image').classList.replace('opacity-100', 'opacity-10');
    document.getElementById('slide_text_box').classList.replace('opacity-100', 'opacity-0');
    await sleep(285);
    slideNext();
}

function slidePrev() {
    slide_pos--;
    if (slide_pos < 0) {
        slide_pos = 5;
    }
    document.getElementById('slide_image').style.backgroundImage = slide_images[slide_pos];
    document.getElementById('slide_text').innerHTML = slide_texts[slide_pos];
}

// slides End

function toggleModal() {
    if (document.getElementById("modal").classList.contains('hidden')) {
        document.getElementById("modal").classList.remove('hidden');
        document.getElementById("modal").classList.add('flex');
        document.getElementById("workspace").classList.add('overflow-y-hidden')
    }
    else {
        reset();
    }
}

function toggleImage() {
    if (document.getElementById("body-image").classList.contains('block')) {
        document.getElementById("body-image").classList.remove('block');
        document.getElementById("body-image").classList.add('hidden');
        document.getElementById("body-text").classList.remove('hidden');
        document.getElementById("body-text").classList.add('block');
    }
    else {
        document.getElementById("body-image").classList.remove('hidden');
        document.getElementById("body-image").classList.add('block');
        document.getElementById("body-text").classList.remove('block');
        document.getElementById("body-text").classList.add('hidden');
    }
}

function toggleButton() {
    if (document.getElementById("button-show").classList.contains('block')) {
        document.getElementById("button-show").classList.remove('block');
        document.getElementById("button-show").classList.add('hidden');
        document.getElementById("body-image").classList.remove('block');
        document.getElementById("body-image").classList.add('hidden');
        document.getElementById("body-text").classList.remove('hidden');
        document.getElementById("body-text").classList.add('block');
    }
    else {
        document.getElementById("button-show").classList.remove('hidden');
        document.getElementById("button-show").classList.add('block');
        document.getElementById("body-image").classList.remove('hidden');
        document.getElementById("body-image").classList.add('block');
        document.getElementById("body-text").classList.remove('block');
        document.getElementById("body-text").classList.add('hidden');
    }
}

function reset() {
    document.getElementById("modal").classList.remove('flex');
    document.getElementById("modal").classList.add('hidden');
    document.getElementById("button-show").classList.remove('hidden');
    document.getElementById("button-show").classList.add('block');
    document.getElementById("body-image").classList.remove('hidden');
    document.getElementById("body-image").classList.add('block');
    document.getElementById("body-text").classList.remove('block');
    document.getElementById("body-text").classList.add('hidden');
    document.getElementById("workspace").classList.remove('overflow-y-hidden')
    candidates_votes = [0,0,0,0,0,0];
    white_null = [0,0];
    result = `Não houve vencedor`;
    candidate_winner_name = ``;
    candidate_winner_amount = 0;
    candidate_winner_index = 6;
    if_candidates_draw = [];
}
