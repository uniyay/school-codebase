var r = document.querySelector(':root')
var rs = getComputedStyle(r)
var dark = localStorage.getItem('dark')
const page = document.body.classList.contains('page1')
const pagenull = document.body.classList.contains('page0')

if (page == true) {
    imgl = "url('../imagens/wd2art_light.jpg')";
    imgd = "url('../imagens/wd2art.jpg')"
}
else if (pagenull == true) {
    imgl = " ";
    imgd = " "
}
else {
    imgl = "url('../imagens/tw3art_light.jpg')";
    imgd = "url('../imagens/tw3art.jpg')"
}

if (dark == 0) {
    r.style.setProperty('--color1', '#fff');
    r.style.setProperty('--color2', '#000');
    r.style.setProperty('--color3', '#1c71d8');
    r.style.setProperty('--color4', '#62a0ea');
    r.style.setProperty('--image', imgl);
    }
else {
    r.style.setProperty('--image', imgd);
    }

function color() {
    var c1 = rs.getPropertyValue('--color1')
    var c2 = rs.getPropertyValue('--color2')
    var c3 = rs.getPropertyValue('--color3')
    var c4 = rs.getPropertyValue('--color4')
    if (c1 == "#fff") {
        r.style.setProperty('--color1', c2);
        r.style.setProperty('--color2', c1);
        r.style.setProperty('--color3', c4);
        r.style.setProperty('--color4', c3);
        r.style.setProperty('--image', imgd);
        localStorage.setItem('dark', 1)
    }
    else {
        r.style.setProperty('--color1', c2);
        r.style.setProperty('--color2', c1);
        r.style.setProperty('--color3', c4);
        r.style.setProperty('--color4', c3);
        r.style.setProperty('--image', imgl);
        localStorage.setItem('dark', 0)
    }
    }