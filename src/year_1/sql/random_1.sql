-- What does this do?
-- ------------------
-- This is a generic sql codesheet to store
-- random commands used in ms sqlserver

CREATE TABLE teacher -- Professor
(
	id_teacher int primary key identity(1,1),

	nomeprof varchar(20),
	cpfprof varchar(20)
)

CREATE TABLE course -- Curso
(
	id_course int primary key identity(1,1),

	course_duration int,
	course_name varchar(30) not null,
	course_acronym varchar(5)
)

CREATE TABLE subjects -- Disciplinas
(
	id_subject int primary key identity(1,1),

	name_subject, id_teacher, id_course varchar(20),

	id_teacher int foreign key references teacher(id_teacher),
	id_course int foreign key references course(id_course)
)

CREATE TABLE student -- Estudantes
(
	id_student int primary key identity(1,1),

	name_student varchar(20),
	cpf_student varchar(20),

	id_course int foreign key references course(id_course)
)

CREATE TABLE student_subjects -- Alunos & Disciplinas
(
	id_student_subjects int primary key,

	student_subject_grade tinyint,
	student_subject_absence tinyint,

	id_student int foreign key references student(id_student),
	id_course int foreign key references course(id_course)
)

exec sp_rename 'course_acronym', 'c_acronym', 'column' --Rename a table column name

insert into course (course_duration,course_name,course_acronym) values (80,'Edificações','Edi')
insert into course (course_duration,course_name,course_acronym) values (80,'Mecânica','Mec')
insert into course (course_duration,course_name,course_acronym) values (80,'Segurança da Vida','SV')
insert into course (course_duration,course_name,course_acronym) values (80,'Medicina','Med')
insert into course (course_duration,course_name,course_acronym) values (80,'Gastrônomia','Gas')
insert into course (course_duration,course_name,course_acronym) values (80,'Mecatrônica','Meca')
insert into course (course_duration,course_name,course_acronym) values (80,'Logístisca','Log')
insert into course (course_duration,course_name,course_acronym) values (80,'Alimentos','Ali')
insert into course (course_duration,course_name,course_acronym) values (80,'Eletrônica','Ele')
insert into course (course_duration,course_name,course_acronym) values (80,'Desenvolvimento de Sistemas','DS')

insert into student(name_student,cpf_student,id_course) values('Vetor','154.884.165-84',10)
insert into student(name_student,cpf_student,id_course) values('Maria','954.884.165-84',2)
insert into student(name_student,cpf_student,id_course) values('João','854.884.165-84',1)
insert into student(name_student,cpf_student,id_course) values('Osvaldo','757.884.165-84',5)
insert into student(name_student,cpf_student,id_course) values('Taline','754.854.165-84',5)
insert into student(name_student,cpf_student,id_course) values('Queiróiz','754.814.165-84',6)
insert into student(name_student,cpf_student,id_course) values('Cristiano','754.882.165-84',7)
insert into student(name_student,cpf_student,id_course) values('Sabrina','754.884.168-84',10)
insert into student(name_student,cpf_student,id_course) values('Amélia','754.884.165-87',9)
insert into student(name_student,cpf_student,id_course) values('Juliano','754.884.165-64',10)

insert into subjects(name_subject, id_teacher, id_course) values('Matemática',1,1)
insert into subjects(name_subject, id_teacher, id_course) values('Português',2,1)
insert into subjects(name_subject, id_teacher, id_course) values('Fisíca',3,3)
insert into subjects(name_subject, id_teacher, id_course) values('Química',4,3)
insert into subjects(name_subject, id_teacher, id_course) values('Inglês',5,1)
insert into subjects(name_subject, id_teacher, id_course) values('Filosofia',6,4)
insert into subjects(name_subject, id_teacher, id_course) values('Geografia',7,5)
insert into subjects(name_subject, id_teacher, id_course) values('Análise de Projeto',8,10)
insert into subjects(name_subject, id_teacher, id_course) values('Espanhol',9,1)
insert into subjects(name_subject, id_teacher, id_course) values('Técnicas de Programação e Algoritímos',10,10)

insert into teacher(nomeprof, cpfprof) values('Goldo','123.456.789-11')
insert into teacher(nomeprof, cpfprof) values('Astolfo','123.456.789-11')
insert into teacher(nomeprof, cpfprof) values('Benedito','123.486.789-11')
insert into teacher(nomeprof, cpfprof) values('Astriade','123.426.789-11')
insert into teacher(nomeprof, cpfprof) values('Renata','123.456.739-11')
insert into teacher(nomeprof, cpfprof) values('Palomar','123.456.789-11')
insert into teacher(nomeprof, cpfprof) values('Pompateur','143.456.789-11')
insert into teacher(nomeprof, cpfprof) values('Marina','123.956.789-11')
insert into teacher(nomeprof, cpfprof) values('Magali','123.456.789-15')
insert into teacher(nomeprof, cpfprof) values('Pietra','123.450.789-11')

insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(2,2,1,10 )
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(23,55,8,10)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(22,34,10,10)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(42,97,2,2)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(562,65,3,1)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(256,123,4,5)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(223,453,5,5)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(2897,213,6,6)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(2234,5,7,7)
insert into student_subjects(student_subject_grade, student_subject_absence, id_student, id_course) values(200,10,9,9)

SELECT *
from student