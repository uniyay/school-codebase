nums = [int(input()) for x in range(3)]

wait = nums[1]
result = 0

while wait <= nums[2]:
    if nums[2] < 100:
        #print ("Menor que 100")
        n_e = wait//10
        n_d = wait%10
        hold = n_e+n_d
        if hold == nums[0]:
            result += 1
        wait += 1
    elif nums[2] < 1000:
        #print ("Menor que 1000")
        n_e = wait//100
        n_c = (wait % 100)//10
        n_d = (wait % 100)%10
        hold = n_e+n_c+n_d
        if hold == nums[0]:
            result += 1
        wait += 1
    elif nums[2] < 10000:
        #print ("Menor que 10000")
        n_e = wait//1000
        n_ec = (wait % 1000)//100
        n_dc = ((wait % 1000) % 100)//10
        n_d = ((wait % 1000) % 100)%10
        hold = n_e+n_ec+n_dc+n_d
        if hold == nums[0]:
            result += 1
        wait += 1
    if nums[2] == 10000:
        #print ("Igual a 10000")
        if 1 == nums[0]:
            result = 5
        wait += nums[2]

print (result)
