num = int(input())
tam = [int(x) for x in input().split(' ')]
quantP = int(input())
quantM = int(input())
p = 0
m = 0

for x in tam:
    if x == 1:
        p += 1
    if x == 2:
        m += 1

if p != quantP or m != quantM:
    final = "N"
else:
    final = "S"

print(final)