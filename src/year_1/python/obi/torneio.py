part = [input() for x in range(6)]
ganhou = int()

for x in part:
    if x == "V":
        ganhou += 1

if ganhou == 5 or ganhou == 6:
    grupo = 1
elif ganhou == 3 or ganhou == 4:
    grupo = 2
elif ganhou == 1 or ganhou == 2:
    grupo = 3
else:
    grupo = -1

print (grupo)