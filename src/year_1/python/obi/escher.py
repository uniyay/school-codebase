quant = int(input())
seq = [int(x) for x in input().split(' ')]
soma = 0

seq_inv = seq.copy()
seq_inv.reverse()

num = seq[0] + seq_inv[0]
for x, y in zip(seq, seq_inv):
    if (x + y) != num:
        print("N")
        break
else:
    print("S")
