# What does this code do?
# -----------------------
# reference: https://olimpiada.ic.unicamp.br/pratique/p1/2020/f1/pandemia/
# original: https://github.com/eloise-takami/pandemia_obi

T, D = [int(i) for i in input().split()]
I, R = [int(i) for i in input().split()]
infect = []
infect.append(I)

for x in range(1,D+1):
    base = [int(y) for y in input().split()[1:]]
    if(x > R-1):
        if any(z in infect for z in base):
            infect.extend([z for z in base if z not in infect])

print(len(infect))
