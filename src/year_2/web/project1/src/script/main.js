systemTheme();

function toggleTheme() {
    if (localStorage.theme === 'light') {
        localStorage.theme = 'dark';
        document.documentElement.classList.add('dark');
        document.getElementById('themeIcon').classList.replace('fa-moon', 'fa-sun');
        document.getElementById('mobileThemeIcon').classList.replace('fa-moon', 'fa-sun');
    }
    else {
        localStorage.theme = 'light';
        document.documentElement.classList.remove('dark');
        document.getElementById('themeIcon').classList.replace('fa-sun', 'fa-moon');
        document.getElementById('mobileThemeIcon').classList.replace('fa-sun', 'fa-moon');
    }
}

function toggleMenu() {
    var menu = document.getElementById('navMobileMenu');
    isHidden = menu.classList.contains('hidden');
    
    if (isHidden) {
        menu.classList.replace('hidden', 'block');
    }
    else {
        menu.classList.replace('block', 'hidden');
    }
}

function systemTheme() {
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark')
        document.getElementById('themeIcon').classList.replace('fa-moon', 'fa-sun');
    }
    else {
        document.documentElement.classList.remove('dark');
        document.getElementById('themeIcon').classList.replace('fa-sun', 'fa-moon');
    }
}