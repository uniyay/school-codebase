/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./index.php",
    "./src/**/*.{html,php}",
    "./pages/*.{html,php}"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

