<!DOCTYPE html>
<html class="dark" lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./../src/styles/main.css">
    <link rel="stylesheet" href="./../src/styles/tailwind.css">
    <link rel="stylesheet" href="./../src/opt/forkawesome/fork-awesome.min.css">
    <link rel="icon" type="image/x-icon" href="./../public/favicon.png">
    <title>Project1</title>
</head>
<body class="antialiased text-sm sm:text-base text-black dark:text-white bg-neutral-50 dark:bg-zinc-800">
    <nav class="h-16 z-50 w-screen grid grid-flow-col items-center sticky border-b dark:border-zinc-500 bg-white dark:bg-zinc-900">
        <a href="./../index.html" class="justify-self-start ml-12 text-2xl font-extrabold">
            Project1
        </a>

        <!-- Default Menu -->
        <div class="justify-self-end mr-12 space-x-4 hidden sm:block">
            <a href="#" class="p-3 hover:px-4 duration-300 border-x border-green-500 hover:text-green-500 hover:bg-green-50 hover:dark:bg-zinc-800">
                Formulário
            </a>
            <button onclick="toggleTheme(); return;" class="p-3 hover:px-4 duration-300 border-x hover:border-blue-500 hover:text-blue-500 hover:bg-sky-50 hover:dark:bg-zinc-800">
                <!-- Change Theme -->
                <i id="themeIcon" class="fa fa-fw fa-sun" aria-hidden="true"></i>
            </button>
        </div>

        <!-- Mobile Menu -->
        <button onclick="toggleMenu();" class="sm:hidden p-3 mr-12 justify-self-end border">
            Menu
        </button>
        <div id="navMobileMenu" class="bg-white dark:bg-zinc-900 w-screen top-[63px] hidden sm:hidden absolute z-40 justify-self-center">
            <button onclick="toggleTheme();" class="p-3 w-screen border-b dark:border-zinc-500">
                Mudar Tema
                <i id="mobileThemeIcon" class="fa fa-fw fa-sun" aria-hidden="true"></i>
            </button>
            <button onclick="location.href='#';" class="p-3 w-screen border-b dark:border-zinc-500">
                Formulário
            </button>
        </div>
    </nav>

    <!-- Elements -->
    <div class="w-screen h-[calc(100vh-4rem)] z-10 py-12">
        <div class="flex justify-center items-center">
            <div class="max-w-md mx-auto text-justify space-y-8 px-4">
                <h1 class="font-extrabold text-4xl text-center">
                    Dados Básicos
                </h1>
                <p>
                    Prencha os dados a seguir de forma correta.
                </p>
                <form action="./user.php" method="get">
                    <div class="mb-6">
                        <label for="first_name" class="block mb-2">Primeiro Nome:</label>
                        <input name="first_name" type="text" id="first_name" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-full bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" required>
                    </div>
                    <div class="mb-6">
                        <label for="last_name" class="block mb-2">Último Nome:</label>
                        <input name="last_name" type="text" id="last_name" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-full bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" required>
                    </div>
                    <div class="mb-6">
                        <label for="date_of_birth" class="block mb-2">Data de Nascimento:</label>
                        <input name="date_of_birth" type="date" id="date_of_birth" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-full bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" required>
                    </div>
                    <div class="mb-6">
                        <p class="mb-2">Sexo:</p>
                        <ul class="flex flex-row justify-center space-x-4">
                            <li class="rounded-full px-8 sm:px-12 py-2 bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white border border-zinc-500">
                                <input id="maleRadio" value="Masculino" name="gender" type="radio" checked required>    
                                <label for="maleRadio">Masculino</label>
                            </li>
                            <li class="rounded-full px-8 sm:px-12 py-2 bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white border border-zinc-500">
                                <input id="femaleRadio" value="Feminino" name="gender" type="radio">    
                                <label for="femaleRadio">Feminino</label>
                            </li>
                        </ul>
                    </div>
                    <div class="mb-6">
                        <label for="height" class="block mb-2">Altura:</label>
                        <input name="height" type="number" step="0.01" min="1" max="2" id="height" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-full bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" placeholder="1.5" required>
                    </div>
                    <div class="mb-6">
                        <label for="email" class="block mb-2">Email:</label>
                        <input name="email" type="email" id="email" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-full bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" placeholder="nome@mail.com" required>
                    </div>
                    <div class="mb-6">
                        <label for="text_area" class="block mb-2">Descrição</label>
                        <textarea name="about" rows="4" id="text_area" class="max-w-md w-full p-2 ring-1 ring-offset-2 dark:ring-offset-zinc-800 ring-blue-500 rounded-lg bg-neutral-100 dark:bg-zinc-700 text-black dark:text-white" placeholder="Escreva sobre você" required></textarea>
                    </div>
                    <div class="mb-12 flex justify-center">
                        <button class="px-10 py-1 rounded-full hover:px-12 duration-300 border bg-blue-300 dark:bg-blue-700 border-blue-400">
                            Enviar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Script -->
    <script type="text/javascript" src="./../src/script/main.js"></script>
</body>
</html>
