<!DOCTYPE html>
<html class="dark" lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./../src/styles/main.css">
    <link rel="stylesheet" href="./../src/styles/tailwind.css">
    <link rel="stylesheet" href="./../src/opt/forkawesome/fork-awesome.min.css">
    <link rel="icon" type="image/x-icon" href="./../public/favicon.png">
    <title>Project1</title>
</head>
<body class="antialiased text-sm sm:text-base text-black dark:text-white bg-neutral-50 dark:bg-zinc-800">
    <nav class="h-16 z-50 w-screen grid grid-flow-col items-center sticky border-b dark:border-zinc-500 bg-white dark:bg-zinc-900">
        <a href="./../index.html" class="justify-self-start ml-12 text-2xl font-extrabold">
            Project1
        </a>

        <!-- Default Menu -->
        <div class="justify-self-end mr-12 space-x-4 hidden sm:block">
            <a href="./form.php" class="p-3 hover:px-4 duration-300 border-x hover:border-blue-500 hover:text-blue-500 hover:bg-green-50 hover:dark:bg-zinc-800">
                Formulário
            </a>
            <button onclick="toggleTheme(); return;" class="p-3 hover:px-4 duration-300 border-x hover:border-blue-500 hover:text-blue-500 hover:bg-sky-50 hover:dark:bg-zinc-800">
                <!-- Change Theme -->
                <i id="themeIcon" class="fa fa-fw fa-sun" aria-hidden="true"></i>
            </button>
        </div>

        <!-- Mobile Menu -->
        <button onclick="toggleMenu();" class="sm:hidden p-3 mr-12 justify-self-end border">
            Menu
        </button>
        <div id="navMobileMenu" class="bg-white dark:bg-zinc-900 w-screen top-[63px] hidden sm:hidden absolute z-40 justify-self-center">
            <button onclick="toggleTheme();" class="p-3 w-screen border-b dark:border-zinc-500">
                Mudar Tema
                <i id="mobileThemeIcon" class="fa fa-fw fa-sun" aria-hidden="true"></i>
            </button>
            <button onclick="location.href='./form.php';" class="p-3 w-screen border-b dark:border-zinc-500">
                Formulário
            </button>
        </div>
    </nav>

    <!-- Elements -->
    <div class="w-screen h-[calc(100vh-4rem)] z-10 py-12">
        <div class="flex justify-center items-center">
            <div class="max-w-md mx-auto space-y-8 px-4 mb-10">
                <h1 class="font-extrabold text-4xl text-center">
                    Valores Preenchidos
                </h1>
                <?php
                    echo "<p>Primeiro nome: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['first_name'] . "</span></p>";
                    echo "<p>Último nome: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['last_name'] . "</span></p>";
                    echo "<p>Nascimento: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['date_of_birth'] . "</span></p>";
                    echo "<p>Sexo: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['gender'] . "</span></p>";
                    echo "<p>Altura: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['height'] . "</span></p>";
                    echo "<p>Email: <span class=\"float-right px-8 py-2 ml-3 bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-full\">" . $_GET['email'] . "</span></p>";
                ?>
                <div>
                    <h2 class="font-bold text-3xl text-center mb-4">
                        Sobre
                    </h2>
                    <p class="px-4 py-2 break-words text-justify bg-zinc-200 dark:bg-zinc-700 border border-zinc-500 rounded-2xl">
                        <?php
                            echo $_GET['about'];
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Script -->
    <script type="text/javascript" src="./../src/script/main.js"></script>
</body>
</html>
