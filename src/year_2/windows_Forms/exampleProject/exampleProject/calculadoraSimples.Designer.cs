﻿namespace tpaProject1
{
    partial class calculadoraSimples
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(calculadoraSimples));
            this.sumButton = new System.Windows.Forms.Button();
            this.subtractButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.powButton = new System.Windows.Forms.Button();
            this.compareButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.oddevenButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.resultMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.xDivisorLabel2 = new System.Windows.Forms.Label();
            this.value2TextBox = new System.Windows.Forms.TextBox();
            this.value1TextBox = new System.Windows.Forms.TextBox();
            this.value1Label = new System.Windows.Forms.Label();
            this.value2Label = new System.Windows.Forms.Label();
            this.xDivisorLabel1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sumButton
            // 
            resources.ApplyResources(this.sumButton, "sumButton");
            this.sumButton.Name = "sumButton";
            this.sumButton.UseVisualStyleBackColor = true;
            this.sumButton.Click += new System.EventHandler(this.sumButton_Click);
            // 
            // subtractButton
            // 
            resources.ApplyResources(this.subtractButton, "subtractButton");
            this.subtractButton.Name = "subtractButton";
            this.subtractButton.UseVisualStyleBackColor = true;
            this.subtractButton.Click += new System.EventHandler(this.subtractButton_Click);
            // 
            // divideButton
            // 
            resources.ApplyResources(this.divideButton, "divideButton");
            this.divideButton.Name = "divideButton";
            this.divideButton.UseVisualStyleBackColor = true;
            this.divideButton.Click += new System.EventHandler(this.divideButton_Click);
            // 
            // multiplyButton
            // 
            resources.ApplyResources(this.multiplyButton, "multiplyButton");
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.UseVisualStyleBackColor = true;
            this.multiplyButton.Click += new System.EventHandler(this.multiplyButton_Click);
            // 
            // powButton
            // 
            resources.ApplyResources(this.powButton, "powButton");
            this.powButton.Name = "powButton";
            this.toolTip.SetToolTip(this.powButton, resources.GetString("powButton.ToolTip"));
            this.powButton.UseVisualStyleBackColor = true;
            this.powButton.Click += new System.EventHandler(this.powButton_Click);
            // 
            // compareButton
            // 
            resources.ApplyResources(this.compareButton, "compareButton");
            this.compareButton.Name = "compareButton";
            this.toolTip.SetToolTip(this.compareButton, resources.GetString("compareButton.ToolTip"));
            this.compareButton.UseVisualStyleBackColor = true;
            this.compareButton.Click += new System.EventHandler(this.compareButton_Click);
            // 
            // clearButton
            // 
            resources.ApplyResources(this.clearButton, "clearButton");
            this.clearButton.Name = "clearButton";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // oddevenButton
            // 
            resources.ApplyResources(this.oddevenButton, "oddevenButton");
            this.oddevenButton.Name = "oddevenButton";
            this.toolTip.SetToolTip(this.oddevenButton, resources.GetString("oddevenButton.ToolTip"));
            this.oddevenButton.UseVisualStyleBackColor = true;
            this.oddevenButton.Click += new System.EventHandler(this.oddevenButton_Click);
            // 
            // resultMaskedTextBox
            // 
            this.resultMaskedTextBox.BeepOnError = true;
            this.resultMaskedTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.resultMaskedTextBox, "resultMaskedTextBox");
            this.resultMaskedTextBox.Name = "resultMaskedTextBox";
            this.resultMaskedTextBox.ReadOnly = true;
            // 
            // xDivisorLabel2
            // 
            this.xDivisorLabel2.BackColor = System.Drawing.SystemColors.ControlDark;
            resources.ApplyResources(this.xDivisorLabel2, "xDivisorLabel2");
            this.xDivisorLabel2.Name = "xDivisorLabel2";
            // 
            // value2TextBox
            // 
            resources.ApplyResources(this.value2TextBox, "value2TextBox");
            this.value2TextBox.Name = "value2TextBox";
            this.value2TextBox.TextChanged += new System.EventHandler(this.valueTextBox_TextChanged);
            this.value2TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.valueTextBox_KeyPress);
            // 
            // value1TextBox
            // 
            resources.ApplyResources(this.value1TextBox, "value1TextBox");
            this.value1TextBox.Name = "value1TextBox";
            this.value1TextBox.TextChanged += new System.EventHandler(this.valueTextBox_TextChanged);
            this.value1TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.valueTextBox_KeyPress);
            // 
            // value1Label
            // 
            resources.ApplyResources(this.value1Label, "value1Label");
            this.value1Label.Name = "value1Label";
            // 
            // value2Label
            // 
            resources.ApplyResources(this.value2Label, "value2Label");
            this.value2Label.Name = "value2Label";
            // 
            // xDivisorLabel1
            // 
            this.xDivisorLabel1.BackColor = System.Drawing.SystemColors.ControlDark;
            resources.ApplyResources(this.xDivisorLabel1, "xDivisorLabel1");
            this.xDivisorLabel1.Name = "xDivisorLabel1";
            // 
            // calculadoraSimples
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.xDivisorLabel1);
            this.Controls.Add(this.value2Label);
            this.Controls.Add(this.value1Label);
            this.Controls.Add(this.value1TextBox);
            this.Controls.Add(this.value2TextBox);
            this.Controls.Add(this.xDivisorLabel2);
            this.Controls.Add(this.resultMaskedTextBox);
            this.Controls.Add(this.oddevenButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.multiplyButton);
            this.Controls.Add(this.powButton);
            this.Controls.Add(this.compareButton);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.subtractButton);
            this.Controls.Add(this.sumButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "calculadoraSimples";
            this.ShowIcon = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button sumButton;
        private System.Windows.Forms.Button subtractButton;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button multiplyButton;
        private System.Windows.Forms.Button powButton;
        private System.Windows.Forms.Button compareButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button oddevenButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.MaskedTextBox resultMaskedTextBox;
        private System.Windows.Forms.Label xDivisorLabel2;
        private System.Windows.Forms.TextBox value2TextBox;
        private System.Windows.Forms.TextBox value1TextBox;
        private System.Windows.Forms.Label value1Label;
        private System.Windows.Forms.Label value2Label;
        private System.Windows.Forms.Label xDivisorLabel1;
    }
}

