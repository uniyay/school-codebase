﻿using System;
using System.Windows.Forms;

namespace tpaProject1
{
    public partial class menuInicial : Form
    {
        public menuInicial()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void calculadoraSimplesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var b = new calculadoraSimples();
            b.ShowDialog();
        }

        private void menuLanchoneteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var b = new menuLanchonete();
            b.ShowDialog();
        }

        private void calculadoraComplexaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var b = new calculadoraComplexa();
            b.ShowDialog();
        }
    }
}
