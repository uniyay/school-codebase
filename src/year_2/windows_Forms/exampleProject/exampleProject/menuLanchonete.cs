﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CheckBox = System.Windows.Forms.CheckBox;
using RadioButton = System.Windows.Forms.RadioButton;

namespace tpaProject1
{
#pragma warning disable IDE1006
    public partial class menuLanchonete : Form
    {
        int order_number = 1;
        double total_price;
        List<string> order_list_GLOBAL = new List<string>();

        Dictionary<string, double> flavor = new Dictionary<string, double>
        {
            { "Ander-Tradicional", 5.5 },
            { "Ander-Salada", 3.35 },
            { "Ander-Bacon", 6 },
            { "Ander-Beyond-Burger©", 8.25 },
            { "Ander-Ovo", 6.25 }
        };

        Dictionary<string, double> drink = new Dictionary<string, double>
        {
            { "Refri Coca-Cola [350ml]", 3.5 },
            { "Refri Fanta-Laranja [350ml]", 3.5 },
            { "Refri Guaraná Antarctica [350ml]", 3 },
            { "Suco de Laranja [500ml]", 5 },
            { "Suco de Uva [500ml]", 5 },
            { "Limonada [500ml]", 5 },
            { "Água [250ml]", 2.5 }
        };
        public menuLanchonete()
        {
            InitializeComponent();
        }
        private void newButton_Click(object sender, EventArgs e)
        {
            double price_buffer = 0.0;

            string order_list = string.Empty;

            //
            // Flavor ComboBox
            //

            double flavor_value = 0.0;
            double flavor_multiplicator = (double)flavorNumericUpDown.Value;
            string flavor_name = string.Empty; // Used in orderDataGridView

            if (flavorComboBox.SelectedIndex > -1) {
                order_list += flavorComboBox.SelectedItem.ToString();
                flavor_name = flavorComboBox.SelectedItem.ToString();
                flavor.TryGetValue(flavorComboBox.SelectedItem.ToString(), out flavor_value); // Price
                flavor_value *= flavor_multiplicator;
            }
            else
            {
                // Cancel the order
                return;
            }

            price_buffer = flavor_value;

            //
            // Bread RadioBoxes
            //

            var checkedButton = breadGroupBox.Controls.OfType<RadioButton>().FirstOrDefault(c => c.Checked);
            string bread_name = checkedButton.Text; // Used in orderDataGridView

            order_list += $" | {bread_name}";

            if (integralBreadCheckBox.Checked)
            {
                price_buffer += 1;  // Price
                order_list += " - Integral";
                bread_name += " - Integral";
            }

            //
            // Condiment CheckBoxes
            //

            int condimentCheckBoxAmount = condimentGroupBox.Controls.OfType<CheckBox>().Count(c => c.Checked);
            int condimentCheckBoxLoopPosition = 0;
            string condiment_name = string.Empty; // Used in orderDataGridView

            foreach (CheckBox checkBox in condimentGroupBox.Controls.OfType<CheckBox>())
            {
                if (checkBox.Checked)
                {
                    price_buffer += 0.25 * flavor_multiplicator; // Price | Multiplicate the value based on the amount of orders

                    if (condimentCheckBoxLoopPosition == 0)
                    {
                        order_list += " c/ ";
                    }

                    order_list += checkBox.Text;
                    condiment_name += checkBox.Text;

                    if (condimentCheckBoxLoopPosition != condimentCheckBoxAmount - 1)
                    {
                        order_list += ", ";
                        condiment_name += ", ";
                    }
                    else
                    {
                        break;
                    }

                    condimentCheckBoxLoopPosition++;
                }

            }

            //
            // Drink ComboBox
            //

            double drink_value = 0.0;
            double drink_multiplicator = (double)drinkNumericUpDown.Value;
            string drink_name = string.Empty; // Used in orderDataGridView

            if (drinkComboBox.SelectedIndex > -1)
            {
                drink_name = drinkComboBox.SelectedItem.ToString();
                order_list += $" | Bebida: {drink_name}";
                drink.TryGetValue(drinkComboBox.SelectedItem.ToString(), out drink_value); // Price
                drink_value *= drink_multiplicator;
            }

            price_buffer += drink_value;

            //
            // Return/Finish order
            //

            if (order_list_GLOBAL.Contains(order_list))
            {
                int index_list = order_list_GLOBAL.IndexOf(order_list);
                
                // amountColumn
                orderDataGridView[2, index_list].Value = Convert.ToUInt16(orderDataGridView[2, index_list].Value) + Convert.ToUInt16(flavorNumericUpDown.Value); // flavor
                orderDataGridView[6, index_list].Value = Convert.ToUInt16(orderDataGridView[6, index_list].Value) + Convert.ToUInt16(drinkNumericUpDown.Value); // drink
                // valueColumn
                orderDataGridView[1, index_list].Value = Convert.ToDouble(orderDataGridView[1, index_list].Value) + price_buffer;
            }
            else
            {
                order_list_GLOBAL.Add(order_list);
                orderDataGridView.Rows.Add(order_number, price_buffer, flavor_multiplicator, flavor_name, bread_name, condiment_name, drink_multiplicator, drink_name); // Create a new row in orderDataGridView
            }

            orderListBox.Items.Add($"-- Pedido N*{order_number}: {order_list} [Valor: R${price_buffer:.00}]"); // Append the new list containing the new order to orderListBox
            
            total_price += price_buffer;
            priceTextBox.Text = $"R${total_price:.00}";

            order_number++; // Changes the order number
            orderTextBox.Text = order_number.ToString(); // Changes the order number in orderTextBox
        }
        private void acceptButton_Click(object sender, EventArgs e)
        {
            if (order_number != 1)
            {
                flavorGroupBox.Enabled = false;
                breadGroupBox.Enabled = false;
                drinkGroupBox.Enabled = false;
                condimentGroupBox.Enabled = false;
                newButton.Enabled = false;
                acceptButton.Enabled = false;
                orderGroupBox.Text = "Total de Pedidos";
                orderTextBox.Text = (order_number - 1).ToString();
                orderListBox.Items.Add($"[Valor Total: R${total_price:.00}]");
            }
        }
        private void resetButton_Click(object sender, EventArgs e)
        {
            string buttonName = ((Button)sender).Name;

            flavorGroupBox.Enabled = true;
            flavorComboBox.SelectedIndex = -1;
            flavorPriceLabel.Text = "R$ 0.00";
            flavorNumericUpDown.Value = 1;

            breadGroupBox.Enabled = true;
            baguetteBreadRatio.Checked = true;
            integralBreadCheckBox.Checked = false;

            drinkGroupBox.Enabled = true;
            drinkComboBox.SelectedIndex = -1;
            drinkPriceLabel.Text = "R$ 0.00";
            drinkNumericUpDown.Value = 1;

            condimentGroupBox.Enabled = true;
            foreach (CheckBox c in condimentGroupBox.Controls.OfType<CheckBox>())
            {
                c.Checked = false;
            }

            if (buttonName == "reloadButton")
            {
                order_number = 1;
                total_price = 0.0;
                priceTextBox.Text = "R$ 0.00";

                newButton.Enabled = true;
                acceptButton.Enabled = true;
                orderGroupBox.Text = "Pedido Atual Número";
                orderTextBox.Text = order_number.ToString();

                orderListBox.Items.Clear();
                orderDataGridView.Rows.Clear();
                order_list_GLOBAL.Clear();
            }
        }
        private void combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedTextComboBox = (string)((ComboBox)sender).SelectedItem;

            try
            {
                if (flavor.TryGetValue(selectedTextComboBox, out double valueFlavor))
                {
                    flavorPriceLabel.Text = $"R$ {valueFlavor:.00}";
                }
                else if (drink.TryGetValue(selectedTextComboBox, out double valueDrink))
                {
                    drinkPriceLabel.Text = $"R$ {valueDrink:.00}";
                }
            }
            catch
            {
                return;
            }
        }
    }
}
