﻿namespace tpaProject1
{
    partial class menuLanchonete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flavorGroupBox = new System.Windows.Forms.GroupBox();
            this.flavorPriceLabel = new System.Windows.Forms.Label();
            this.flavorNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.flavorComboBox = new System.Windows.Forms.ComboBox();
            this.breadGroupBox = new System.Windows.Forms.GroupBox();
            this.integralBreadCheckBox = new System.Windows.Forms.CheckBox();
            this.formaBreadRatio = new System.Windows.Forms.RadioButton();
            this.hamburgerBreadRatio = new System.Windows.Forms.RadioButton();
            this.baguetteBreadRatio = new System.Windows.Forms.RadioButton();
            this.drinkGroupBox = new System.Windows.Forms.GroupBox();
            this.drinkPriceLabel = new System.Windows.Forms.Label();
            this.drinkNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.drinkComboBox = new System.Windows.Forms.ComboBox();
            this.condimentGroupBox = new System.Windows.Forms.GroupBox();
            this.catupiryCondimentCheckBox = new System.Windows.Forms.CheckBox();
            this.mustardCondimentCheckBox = new System.Windows.Forms.CheckBox();
            this.ketchupCondimentCheckBox = new System.Windows.Forms.CheckBox();
            this.mayoCondimentCheckBox = new System.Windows.Forms.CheckBox();
            this.horizontalDivisor1 = new System.Windows.Forms.Label();
            this.horizontalDivisor2 = new System.Windows.Forms.Label();
            this.orderListBox = new System.Windows.Forms.ListBox();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.orderGroupBox = new System.Windows.Forms.GroupBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.newButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.reloadButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.orderDataGridView = new System.Windows.Forms.DataGridView();
            this.orderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flavorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.breadColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condimentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drinkAmountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drinkColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.listTabPage = new System.Windows.Forms.TabPage();
            this.tableTabPage = new System.Windows.Forms.TabPage();
            this.priceGroupBox = new System.Windows.Forms.GroupBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.menuLanchoneteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flavorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flavorNumericUpDown)).BeginInit();
            this.breadGroupBox.SuspendLayout();
            this.drinkGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drinkNumericUpDown)).BeginInit();
            this.condimentGroupBox.SuspendLayout();
            this.orderGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.listTabPage.SuspendLayout();
            this.tableTabPage.SuspendLayout();
            this.priceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menuLanchoneteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // flavorGroupBox
            // 
            this.flavorGroupBox.Controls.Add(this.flavorPriceLabel);
            this.flavorGroupBox.Controls.Add(this.flavorNumericUpDown);
            this.flavorGroupBox.Controls.Add(this.flavorComboBox);
            this.flavorGroupBox.Location = new System.Drawing.Point(12, 101);
            this.flavorGroupBox.Name = "flavorGroupBox";
            this.flavorGroupBox.Size = new System.Drawing.Size(206, 64);
            this.flavorGroupBox.TabIndex = 1;
            this.flavorGroupBox.TabStop = false;
            this.flavorGroupBox.Text = "Sabor do Lanche";
            // 
            // flavorPriceLabel
            // 
            this.flavorPriceLabel.AutoSize = true;
            this.flavorPriceLabel.Location = new System.Drawing.Point(6, 16);
            this.flavorPriceLabel.Name = "flavorPriceLabel";
            this.flavorPriceLabel.Size = new System.Drawing.Size(45, 13);
            this.flavorPriceLabel.TabIndex = 3;
            this.flavorPriceLabel.Text = "R$ 0.00";
            // 
            // flavorNumericUpDown
            // 
            this.flavorNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.flavorNumericUpDown.Location = new System.Drawing.Point(165, 32);
            this.flavorNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.flavorNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.flavorNumericUpDown.Name = "flavorNumericUpDown";
            this.flavorNumericUpDown.Size = new System.Drawing.Size(35, 21);
            this.flavorNumericUpDown.TabIndex = 2;
            this.flavorNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // flavorComboBox
            // 
            this.flavorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.flavorComboBox.FormattingEnabled = true;
            this.flavorComboBox.Items.AddRange(new object[] {
            "Ander-Tradicional",
            "Ander-Salada",
            "Ander-Bacon",
            "Ander-Beyond-Burger©",
            "Ander-Ovo"});
            this.flavorComboBox.Location = new System.Drawing.Point(6, 32);
            this.flavorComboBox.Name = "flavorComboBox";
            this.flavorComboBox.Size = new System.Drawing.Size(153, 21);
            this.flavorComboBox.TabIndex = 0;
            this.flavorComboBox.SelectedIndexChanged += new System.EventHandler(this.combobox_SelectedIndexChanged);
            // 
            // breadGroupBox
            // 
            this.breadGroupBox.Controls.Add(this.integralBreadCheckBox);
            this.breadGroupBox.Controls.Add(this.formaBreadRatio);
            this.breadGroupBox.Controls.Add(this.hamburgerBreadRatio);
            this.breadGroupBox.Controls.Add(this.baguetteBreadRatio);
            this.breadGroupBox.Location = new System.Drawing.Point(12, 171);
            this.breadGroupBox.Name = "breadGroupBox";
            this.breadGroupBox.Size = new System.Drawing.Size(206, 115);
            this.breadGroupBox.TabIndex = 2;
            this.breadGroupBox.TabStop = false;
            this.breadGroupBox.Text = "Tipo de Pão";
            // 
            // integralBreadCheckBox
            // 
            this.integralBreadCheckBox.AutoSize = true;
            this.integralBreadCheckBox.Location = new System.Drawing.Point(7, 92);
            this.integralBreadCheckBox.Name = "integralBreadCheckBox";
            this.integralBreadCheckBox.Size = new System.Drawing.Size(61, 17);
            this.integralBreadCheckBox.TabIndex = 3;
            this.integralBreadCheckBox.Text = "Integral";
            this.integralBreadCheckBox.UseVisualStyleBackColor = true;
            // 
            // formaBreadRatio
            // 
            this.formaBreadRatio.AutoSize = true;
            this.formaBreadRatio.Location = new System.Drawing.Point(7, 68);
            this.formaBreadRatio.Name = "formaBreadRatio";
            this.formaBreadRatio.Size = new System.Drawing.Size(54, 17);
            this.formaBreadRatio.TabIndex = 2;
            this.formaBreadRatio.Text = "Forma";
            this.formaBreadRatio.UseVisualStyleBackColor = true;
            // 
            // hamburgerBreadRatio
            // 
            this.hamburgerBreadRatio.AutoSize = true;
            this.hamburgerBreadRatio.Location = new System.Drawing.Point(7, 44);
            this.hamburgerBreadRatio.Name = "hamburgerBreadRatio";
            this.hamburgerBreadRatio.Size = new System.Drawing.Size(77, 17);
            this.hamburgerBreadRatio.TabIndex = 1;
            this.hamburgerBreadRatio.Text = "Hamburger";
            this.hamburgerBreadRatio.UseVisualStyleBackColor = true;
            // 
            // baguetteBreadRatio
            // 
            this.baguetteBreadRatio.AutoSize = true;
            this.baguetteBreadRatio.Checked = true;
            this.baguetteBreadRatio.Location = new System.Drawing.Point(7, 20);
            this.baguetteBreadRatio.Name = "baguetteBreadRatio";
            this.baguetteBreadRatio.Size = new System.Drawing.Size(68, 17);
            this.baguetteBreadRatio.TabIndex = 0;
            this.baguetteBreadRatio.TabStop = true;
            this.baguetteBreadRatio.Text = "Baguette";
            this.baguetteBreadRatio.UseVisualStyleBackColor = true;
            // 
            // drinkGroupBox
            // 
            this.drinkGroupBox.Controls.Add(this.drinkPriceLabel);
            this.drinkGroupBox.Controls.Add(this.drinkNumericUpDown);
            this.drinkGroupBox.Controls.Add(this.drinkComboBox);
            this.drinkGroupBox.Location = new System.Drawing.Point(237, 101);
            this.drinkGroupBox.Name = "drinkGroupBox";
            this.drinkGroupBox.Size = new System.Drawing.Size(206, 64);
            this.drinkGroupBox.TabIndex = 3;
            this.drinkGroupBox.TabStop = false;
            this.drinkGroupBox.Text = "Bebida";
            // 
            // drinkPriceLabel
            // 
            this.drinkPriceLabel.AutoSize = true;
            this.drinkPriceLabel.Location = new System.Drawing.Point(6, 16);
            this.drinkPriceLabel.Name = "drinkPriceLabel";
            this.drinkPriceLabel.Size = new System.Drawing.Size(45, 13);
            this.drinkPriceLabel.TabIndex = 4;
            this.drinkPriceLabel.Text = "R$ 0.00";
            // 
            // drinkNumericUpDown
            // 
            this.drinkNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.drinkNumericUpDown.Location = new System.Drawing.Point(165, 33);
            this.drinkNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.drinkNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.drinkNumericUpDown.Name = "drinkNumericUpDown";
            this.drinkNumericUpDown.Size = new System.Drawing.Size(35, 21);
            this.drinkNumericUpDown.TabIndex = 1;
            this.drinkNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // drinkComboBox
            // 
            this.drinkComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drinkComboBox.FormattingEnabled = true;
            this.drinkComboBox.Items.AddRange(new object[] {
            "Refri Coca-Cola [350ml]",
            "Refri Fanta-Laranja [350ml]",
            "Refri Guaraná Antarctica [350ml]",
            "Suco de Laranja [500ml]",
            "Suco de Uva [500ml]",
            "Limonada [500ml]",
            "Água [250ml]"});
            this.drinkComboBox.Location = new System.Drawing.Point(7, 33);
            this.drinkComboBox.Name = "drinkComboBox";
            this.drinkComboBox.Size = new System.Drawing.Size(152, 21);
            this.drinkComboBox.TabIndex = 0;
            this.drinkComboBox.SelectedIndexChanged += new System.EventHandler(this.combobox_SelectedIndexChanged);
            // 
            // condimentGroupBox
            // 
            this.condimentGroupBox.Controls.Add(this.catupiryCondimentCheckBox);
            this.condimentGroupBox.Controls.Add(this.mustardCondimentCheckBox);
            this.condimentGroupBox.Controls.Add(this.ketchupCondimentCheckBox);
            this.condimentGroupBox.Controls.Add(this.mayoCondimentCheckBox);
            this.condimentGroupBox.Location = new System.Drawing.Point(237, 171);
            this.condimentGroupBox.Name = "condimentGroupBox";
            this.condimentGroupBox.Size = new System.Drawing.Size(206, 115);
            this.condimentGroupBox.TabIndex = 4;
            this.condimentGroupBox.TabStop = false;
            this.condimentGroupBox.Text = "Condimentos";
            // 
            // catupiryCondimentCheckBox
            // 
            this.catupiryCondimentCheckBox.AutoSize = true;
            this.catupiryCondimentCheckBox.Location = new System.Drawing.Point(7, 91);
            this.catupiryCondimentCheckBox.Name = "catupiryCondimentCheckBox";
            this.catupiryCondimentCheckBox.Size = new System.Drawing.Size(64, 17);
            this.catupiryCondimentCheckBox.TabIndex = 3;
            this.catupiryCondimentCheckBox.Text = "Catupiry";
            this.catupiryCondimentCheckBox.UseVisualStyleBackColor = true;
            // 
            // mustardCondimentCheckBox
            // 
            this.mustardCondimentCheckBox.AutoSize = true;
            this.mustardCondimentCheckBox.Location = new System.Drawing.Point(7, 68);
            this.mustardCondimentCheckBox.Name = "mustardCondimentCheckBox";
            this.mustardCondimentCheckBox.Size = new System.Drawing.Size(70, 17);
            this.mustardCondimentCheckBox.TabIndex = 2;
            this.mustardCondimentCheckBox.Text = "Mostarda";
            this.mustardCondimentCheckBox.UseVisualStyleBackColor = true;
            // 
            // ketchupCondimentCheckBox
            // 
            this.ketchupCondimentCheckBox.AutoSize = true;
            this.ketchupCondimentCheckBox.Location = new System.Drawing.Point(7, 44);
            this.ketchupCondimentCheckBox.Name = "ketchupCondimentCheckBox";
            this.ketchupCondimentCheckBox.Size = new System.Drawing.Size(66, 17);
            this.ketchupCondimentCheckBox.TabIndex = 1;
            this.ketchupCondimentCheckBox.Text = "Ketchup";
            this.ketchupCondimentCheckBox.UseVisualStyleBackColor = true;
            // 
            // mayoCondimentCheckBox
            // 
            this.mayoCondimentCheckBox.AutoSize = true;
            this.mayoCondimentCheckBox.Location = new System.Drawing.Point(7, 20);
            this.mayoCondimentCheckBox.Name = "mayoCondimentCheckBox";
            this.mayoCondimentCheckBox.Size = new System.Drawing.Size(72, 17);
            this.mayoCondimentCheckBox.TabIndex = 0;
            this.mayoCondimentCheckBox.Text = "Maionese";
            this.mayoCondimentCheckBox.UseVisualStyleBackColor = true;
            // 
            // horizontalDivisor1
            // 
            this.horizontalDivisor1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.horizontalDivisor1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.horizontalDivisor1.Location = new System.Drawing.Point(-3, 23);
            this.horizontalDivisor1.Name = "horizontalDivisor1";
            this.horizontalDivisor1.Size = new System.Drawing.Size(461, 1);
            this.horizontalDivisor1.TabIndex = 12;
            this.horizontalDivisor1.Text = "label1";
            // 
            // horizontalDivisor2
            // 
            this.horizontalDivisor2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.horizontalDivisor2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.horizontalDivisor2.Location = new System.Drawing.Point(-3, 87);
            this.horizontalDivisor2.Name = "horizontalDivisor2";
            this.horizontalDivisor2.Size = new System.Drawing.Size(461, 1);
            this.horizontalDivisor2.TabIndex = 13;
            this.horizontalDivisor2.Text = "label1";
            // 
            // orderListBox
            // 
            this.orderListBox.FormattingEnabled = true;
            this.orderListBox.HorizontalScrollbar = true;
            this.orderListBox.Location = new System.Drawing.Point(-4, -1);
            this.orderListBox.Name = "orderListBox";
            this.orderListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.orderListBox.Size = new System.Drawing.Size(431, 212);
            this.orderListBox.TabIndex = 14;
            // 
            // orderTextBox
            // 
            this.orderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.orderTextBox.Location = new System.Drawing.Point(6, 19);
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.ReadOnly = true;
            this.orderTextBox.Size = new System.Drawing.Size(112, 32);
            this.orderTextBox.TabIndex = 15;
            this.orderTextBox.Text = "1";
            // 
            // orderGroupBox
            // 
            this.orderGroupBox.Controls.Add(this.orderTextBox);
            this.orderGroupBox.Location = new System.Drawing.Point(12, 27);
            this.orderGroupBox.Name = "orderGroupBox";
            this.orderGroupBox.Size = new System.Drawing.Size(124, 57);
            this.orderGroupBox.TabIndex = 16;
            this.orderGroupBox.TabStop = false;
            this.orderGroupBox.Text = "Pedido Atual Número:";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.titleLabel.Location = new System.Drawing.Point(273, 45);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(170, 24);
            this.titleLabel.TabIndex = 17;
            this.titleLabel.Text = "Anderson Lanches";
            // 
            // newButton
            // 
            this.newButton.Location = new System.Drawing.Point(12, 324);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(159, 41);
            this.newButton.TabIndex = 18;
            this.newButton.Text = "Confirmar";
            this.toolTip.SetToolTip(this.newButton, "Confirmar pedido atual e abrir um novo");
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(237, 324);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(159, 41);
            this.acceptButton.TabIndex = 19;
            this.acceptButton.Text = "Concluir";
            this.toolTip.SetToolTip(this.acceptButton, "Confirmar todos os pedidos e receber o valor total");
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // reloadButton
            // 
            this.reloadButton.BackColor = System.Drawing.Color.MistyRose;
            this.reloadButton.ForeColor = System.Drawing.Color.Chocolate;
            this.reloadButton.Location = new System.Drawing.Point(402, 324);
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.Size = new System.Drawing.Size(41, 41);
            this.reloadButton.TabIndex = 21;
            this.reloadButton.Text = "R";
            this.toolTip.SetToolTip(this.reloadButton, "Reiniciar");
            this.reloadButton.UseVisualStyleBackColor = false;
            this.reloadButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(177, 324);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(41, 41);
            this.clearButton.TabIndex = 24;
            this.clearButton.Text = "C";
            this.toolTip.SetToolTip(this.clearButton, "Limpar Seleções");
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // orderDataGridView
            // 
            this.orderDataGridView.AllowUserToDeleteRows = false;
            this.orderDataGridView.AllowUserToResizeRows = false;
            this.orderDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderColumn,
            this.valueColumn,
            this.amountColumn,
            this.flavorColumn,
            this.breadColumn,
            this.condimentColumn,
            this.drinkAmountColumn,
            this.drinkColumn});
            this.orderDataGridView.Location = new System.Drawing.Point(-3, -3);
            this.orderDataGridView.Name = "orderDataGridView";
            this.orderDataGridView.RowHeadersVisible = false;
            this.orderDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.orderDataGridView.Size = new System.Drawing.Size(429, 216);
            this.orderDataGridView.TabIndex = 20;
            // 
            // orderColumn
            // 
            this.orderColumn.FillWeight = 50F;
            this.orderColumn.HeaderText = "N*";
            this.orderColumn.Name = "orderColumn";
            this.orderColumn.ReadOnly = true;
            this.orderColumn.ToolTipText = "Número do Pedido";
            this.orderColumn.Width = 50;
            // 
            // valueColumn
            // 
            this.valueColumn.HeaderText = "Valor (R$)";
            this.valueColumn.Name = "valueColumn";
            this.valueColumn.ReadOnly = true;
            // 
            // amountColumn
            // 
            this.amountColumn.HeaderText = "Quantidade";
            this.amountColumn.Name = "amountColumn";
            // 
            // flavorColumn
            // 
            this.flavorColumn.HeaderText = "Sabor";
            this.flavorColumn.MinimumWidth = 150;
            this.flavorColumn.Name = "flavorColumn";
            this.flavorColumn.ReadOnly = true;
            this.flavorColumn.Width = 150;
            // 
            // breadColumn
            // 
            this.breadColumn.HeaderText = "Tipo de Pão";
            this.breadColumn.Name = "breadColumn";
            this.breadColumn.ReadOnly = true;
            // 
            // condimentColumn
            // 
            this.condimentColumn.HeaderText = "Condimento(s)";
            this.condimentColumn.MinimumWidth = 200;
            this.condimentColumn.Name = "condimentColumn";
            this.condimentColumn.ReadOnly = true;
            this.condimentColumn.Width = 200;
            // 
            // drinkAmountColumn
            // 
            this.drinkAmountColumn.HeaderText = "Quantidade Bebidas";
            this.drinkAmountColumn.Name = "drinkAmountColumn";
            this.drinkAmountColumn.ReadOnly = true;
            this.drinkAmountColumn.Width = 150;
            // 
            // drinkColumn
            // 
            this.drinkColumn.HeaderText = "Bebida";
            this.drinkColumn.MinimumWidth = 175;
            this.drinkColumn.Name = "drinkColumn";
            this.drinkColumn.ReadOnly = true;
            this.drinkColumn.Width = 175;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.listTabPage);
            this.tabControl1.Controls.Add(this.tableTabPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 380);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(431, 235);
            this.tabControl1.TabIndex = 22;
            // 
            // listTabPage
            // 
            this.listTabPage.Controls.Add(this.orderListBox);
            this.listTabPage.Location = new System.Drawing.Point(4, 22);
            this.listTabPage.Name = "listTabPage";
            this.listTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.listTabPage.Size = new System.Drawing.Size(423, 209);
            this.listTabPage.TabIndex = 0;
            this.listTabPage.Text = "Lista";
            this.listTabPage.UseVisualStyleBackColor = true;
            // 
            // tableTabPage
            // 
            this.tableTabPage.Controls.Add(this.orderDataGridView);
            this.tableTabPage.Location = new System.Drawing.Point(4, 22);
            this.tableTabPage.Name = "tableTabPage";
            this.tableTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tableTabPage.Size = new System.Drawing.Size(423, 209);
            this.tableTabPage.TabIndex = 1;
            this.tableTabPage.Text = "Tabela";
            this.tableTabPage.UseVisualStyleBackColor = true;
            // 
            // priceGroupBox
            // 
            this.priceGroupBox.Controls.Add(this.priceTextBox);
            this.priceGroupBox.Location = new System.Drawing.Point(142, 27);
            this.priceGroupBox.Name = "priceGroupBox";
            this.priceGroupBox.Size = new System.Drawing.Size(119, 57);
            this.priceGroupBox.TabIndex = 23;
            this.priceGroupBox.TabStop = false;
            this.priceGroupBox.Text = "Total:";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.priceTextBox.Location = new System.Drawing.Point(6, 19);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.ReadOnly = true;
            this.priceTextBox.Size = new System.Drawing.Size(107, 32);
            this.priceTextBox.TabIndex = 0;
            this.priceTextBox.Text = "R$ 0.00";
            // 
            // menuLanchoneteBindingSource
            // 
            this.menuLanchoneteBindingSource.DataSource = typeof(tpaProject1.menuLanchonete);
            // 
            // menuLanchonete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 627);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.priceGroupBox);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.reloadButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.newButton);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.orderGroupBox);
            this.Controls.Add(this.horizontalDivisor2);
            this.Controls.Add(this.horizontalDivisor1);
            this.Controls.Add(this.condimentGroupBox);
            this.Controls.Add(this.drinkGroupBox);
            this.Controls.Add(this.breadGroupBox);
            this.Controls.Add(this.flavorGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "menuLanchonete";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "menuLanchonete";
            this.flavorGroupBox.ResumeLayout(false);
            this.flavorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flavorNumericUpDown)).EndInit();
            this.breadGroupBox.ResumeLayout(false);
            this.breadGroupBox.PerformLayout();
            this.drinkGroupBox.ResumeLayout(false);
            this.drinkGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drinkNumericUpDown)).EndInit();
            this.condimentGroupBox.ResumeLayout(false);
            this.condimentGroupBox.PerformLayout();
            this.orderGroupBox.ResumeLayout(false);
            this.orderGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.listTabPage.ResumeLayout(false);
            this.tableTabPage.ResumeLayout(false);
            this.priceGroupBox.ResumeLayout(false);
            this.priceGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menuLanchoneteBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox flavorGroupBox;
        private System.Windows.Forms.ComboBox flavorComboBox;
        private System.Windows.Forms.GroupBox breadGroupBox;
        private System.Windows.Forms.RadioButton formaBreadRatio;
        private System.Windows.Forms.RadioButton hamburgerBreadRatio;
        private System.Windows.Forms.RadioButton baguetteBreadRatio;
        private System.Windows.Forms.GroupBox drinkGroupBox;
        private System.Windows.Forms.ComboBox drinkComboBox;
        private System.Windows.Forms.CheckBox integralBreadCheckBox;
        private System.Windows.Forms.GroupBox condimentGroupBox;
        private System.Windows.Forms.CheckBox catupiryCondimentCheckBox;
        private System.Windows.Forms.CheckBox mustardCondimentCheckBox;
        private System.Windows.Forms.CheckBox ketchupCondimentCheckBox;
        private System.Windows.Forms.CheckBox mayoCondimentCheckBox;
        private System.Windows.Forms.Label horizontalDivisor1;
        private System.Windows.Forms.Label horizontalDivisor2;
        private System.Windows.Forms.ListBox orderListBox;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.GroupBox orderGroupBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.DataGridView orderDataGridView;
        private System.Windows.Forms.BindingSource menuLanchoneteBindingSource;
        private System.Windows.Forms.Button reloadButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage listTabPage;
        private System.Windows.Forms.TabPage tableTabPage;
        private System.Windows.Forms.GroupBox priceGroupBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.NumericUpDown flavorNumericUpDown;
        private System.Windows.Forms.NumericUpDown drinkNumericUpDown;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flavorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn breadColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn condimentColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn drinkAmountColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn drinkColumn;
        private System.Windows.Forms.Label flavorPriceLabel;
        private System.Windows.Forms.Label drinkPriceLabel;
    }
}