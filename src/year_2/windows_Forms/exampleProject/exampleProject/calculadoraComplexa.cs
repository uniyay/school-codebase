﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace tpaProject1
{
#pragma warning disable IDE1006
    public partial class calculadoraComplexa : Form
    {
        bool errorDetected = false;
        public calculadoraComplexa()
        {
            InitializeComponent();
        }
        private void buttonClick(object sender, EventArgs e)
        {
            if (errorDetected == true)
            {
                inputTextBox.Text = string.Empty;
                errorDetected = false;
            }
            string text = ((Button)sender).Text;
            inputTextBox.Text = inputTextBox.Text.Insert(inputTextBox.TextLength, text);
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            inputTextBox.Text = string.Empty;
        }
        private void buttonEquals_Click(object sender, EventArgs e)
        {
            calculate();
        }
        private void inputTextBox_Enter(object sender, EventArgs e)
        {
            if (inputTextBox.Text == "0")
            {
                inputTextBox.Text = string.Empty;
            }
            if (errorDetected == true)
            {
                inputTextBox.Text = string.Empty;
                errorDetected = false;
            }
        }
        private void inputTextBox_Leave(object sender, EventArgs e)
        {
            if (inputTextBox.Text == "0")
            {
                inputTextBox.Text = string.Empty;
            }
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (inputTextBox.Text.Length > 0)
            {
                inputTextBox.Text = inputTextBox.Text.Remove(inputTextBox.TextLength-1);
            }
        }
        private void calculadoraComplexa_KeyDown(object sender, KeyEventArgs e)
        {
            if (inputTextBox.Focused == false)
            {
                inputTextBox.SelectionStart = inputTextBox.Text.Length;

                if (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)
                {
                    inputTextBox.Text = inputTextBox.Text.Insert(inputTextBox.TextLength, (e.KeyValue - 96).ToString());
                }
                if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9)
                {
                    inputTextBox.Text = inputTextBox.Text.Insert(inputTextBox.TextLength, e.KeyCode.ToString().Substring(1));
                }
                if (e.Shift && e.KeyCode == Keys.D6)
                {
                    inputTextBox.Text = inputTextBox.Text.Insert(inputTextBox.TextLength, "^");
                }
                if (e.Shift && e.KeyCode == Keys.D9)
                {
                    inputTextBox.Text = inputTextBox.Text.Insert(inputTextBox.TextLength, "(");
                }
                if (e.Shift && e.KeyCode == Keys.D0)
                {
                    inputTextBox.Text += ")";
                }
                if (e.KeyCode == Keys.Decimal || e.KeyCode == Keys.OemPeriod)
                {
                    inputTextBox.Text += ".";
                }
                if (e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus)
                {
                    inputTextBox.Text += "-";
                }
                if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
                {
                    inputTextBox.Text += "+";
                }
                if (e.KeyCode == Keys.Divide)
                {
                    inputTextBox.Text += "/";
                }
                if (e.KeyCode == Keys.Multiply)
                {
                    inputTextBox.Text += "*";
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (inputTextBox.Text.Length > 0)
                    {
                        inputTextBox.Text = inputTextBox.Text.Remove(inputTextBox.TextLength - 1);
                    }
                }
                if (e.KeyCode == Keys.C)
                {
                    inputTextBox.Text = string.Empty;
                }
                if (e.Control && e.KeyCode == Keys.Enter)
                {
                    calculate();
                }
            }
        }
        private void calculate()
        {
            string expression = string.Empty;
            if (inputTextBox.Text == string.Empty) { expression = "0"; }
            else { expression = convertExpression(inputTextBox.Text); }

            inputTextBox.Text = expression;
        }
        private string convertExpression(string stringExpression)
        {
            List<string> charList = Regex.Split(stringExpression, @"([()\^\/\%\*]|(?<!E)[\+\-])").ToList(); //StackOverflow 4680128 - REGEX
            charList.RemoveAll(item => item == "");
            charList.Insert(0, "0");
            charList.Add("+");
            charList.Add("0");

            while (charList.Count > 1)
            {
                try
                {
                    if (charList.IndexOf("(") > -1)
                    {
                        int pareIndexStart = charList.IndexOf("(");
                        int pareIndexEnd = 0;

                        string expression = string.Empty;
                        int loop_count = 1;
                        for (int i = charList.IndexOf("(") + 1; i < charList.Count(); i++)
                        {
                            pareIndexEnd = i;
                            if (charList[i] == "(")
                            {
                                loop_count++;
                            }
                            else if (charList[i] == ")")
                            {
                                loop_count--;
                            }
                            if (loop_count == 0)
                            {
                                break;
                            }
                            else
                            {
                                expression += charList[i];
                            }
                        }

                        charList[pareIndexStart] = convertExpression(expression);
                        charList.RemoveRange(pareIndexStart+1, pareIndexEnd - pareIndexStart);
                        continue;
                    }
                    if (charList.IndexOf("^") > -1)
                    {
                        int powIndex = charList.IndexOf("^");
                        charList[powIndex - 1] = Math.Pow(double.Parse(charList[powIndex - 1]), double.Parse(charList[powIndex + 1])).ToString();
                        charList.RemoveRange(powIndex, 2);
                        continue;
                    }
                    if (charList.IndexOf("*") > -1 || charList.IndexOf("/") > -1 || charList.IndexOf("%") > -1)
                    {
                        int diviIndex = charList.IndexOf("/");
                        int multIndex = charList.IndexOf("*");
                        int moduIndex = charList.IndexOf("%");

                        if (diviIndex != -1)
                        {
                            charList[diviIndex - 1] = (double.Parse(charList[diviIndex - 1]) / double.Parse(charList[diviIndex + 1])).ToString();
                            charList.RemoveRange(diviIndex, 2);
                        }
                        else if (multIndex != -1)
                        {
                            charList[multIndex - 1] = (double.Parse(charList[multIndex - 1]) * double.Parse(charList[multIndex + 1])).ToString();
                            charList.RemoveRange(multIndex, 2);
                        }
                        else if (moduIndex != -1)
                        {
                            charList[moduIndex - 1] = (double.Parse(charList[moduIndex - 1]) % double.Parse(charList[moduIndex + 1])).ToString();
                            charList.RemoveRange(moduIndex, 2);
                        }
                        continue;
                    }
                    if (charList.IndexOf("+") > -1 || charList.IndexOf("-") > -1)
                    {
                        int sumIndex = charList.IndexOf("+");
                        int subIndex = charList.IndexOf("-");

                        if (subIndex != -1)
                        {
                            charList[subIndex - 1] = (double.Parse(charList[subIndex - 1]) - double.Parse(charList[subIndex + 1])).ToString();
                            charList.RemoveRange(subIndex, 2);
                        }
                        else if (sumIndex != -1)
                        {
                            charList[sumIndex - 1] = (double.Parse(charList[sumIndex - 1]) + double.Parse(charList[sumIndex + 1])).ToString();
                            charList.RemoveRange(sumIndex, 2);
                        }
                        continue;

                    }
                    if (charList[0] == "0")
                    {
                        charList.RemoveAt(0);
                    }
                }
                catch (Exception error)
                {
                    errorDetected = true;
                    return error.GetType().Name;
                }
            }
            return charList[0];
        }
    }
}
