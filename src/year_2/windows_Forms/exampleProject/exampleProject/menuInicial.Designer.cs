﻿namespace tpaProject1
{
    partial class menuInicial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculadoraComplexaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculadoraSimplesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLanchoneteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalDivisor1 = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(339, 24);
            this.menuStrip.TabIndex = 12;
            this.menuStrip.Text = "menuStrip1";
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sobreToolStripMenuItem.Text = "Sobre";
            // 
            // applicationsToolStripMenuItem
            // 
            this.applicationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculadoraComplexaToolStripMenuItem,
            this.calculadoraSimplesToolStripMenuItem,
            this.menuLanchoneteToolStripMenuItem});
            this.applicationsToolStripMenuItem.Name = "applicationsToolStripMenuItem";
            this.applicationsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.applicationsToolStripMenuItem.Text = "Aplicações";
            // 
            // calculadoraComplexaToolStripMenuItem
            // 
            this.calculadoraComplexaToolStripMenuItem.Name = "calculadoraComplexaToolStripMenuItem";
            this.calculadoraComplexaToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.calculadoraComplexaToolStripMenuItem.Text = "calculadoraComplexa";
            this.calculadoraComplexaToolStripMenuItem.Click += new System.EventHandler(this.calculadoraComplexaToolStripMenuItem_Click);
            // 
            // calculadoraSimplesToolStripMenuItem
            // 
            this.calculadoraSimplesToolStripMenuItem.Name = "calculadoraSimplesToolStripMenuItem";
            this.calculadoraSimplesToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.calculadoraSimplesToolStripMenuItem.Text = "calculadoraSimples";
            this.calculadoraSimplesToolStripMenuItem.Click += new System.EventHandler(this.calculadoraSimplesToolStripMenuItem_Click);
            // 
            // menuLanchoneteToolStripMenuItem
            // 
            this.menuLanchoneteToolStripMenuItem.Name = "menuLanchoneteToolStripMenuItem";
            this.menuLanchoneteToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.menuLanchoneteToolStripMenuItem.Text = "menuLanchonete";
            this.menuLanchoneteToolStripMenuItem.Click += new System.EventHandler(this.menuLanchoneteToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Sair";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // horizontalDivisor1
            // 
            this.horizontalDivisor1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.horizontalDivisor1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.horizontalDivisor1.Location = new System.Drawing.Point(0, 24);
            this.horizontalDivisor1.Name = "horizontalDivisor1";
            this.horizontalDivisor1.Size = new System.Drawing.Size(5000, 1);
            this.horizontalDivisor1.TabIndex = 13;
            this.horizontalDivisor1.Text = "label1";
            // 
            // menuInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(339, 450);
            this.Controls.Add(this.horizontalDivisor1);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "menuInicial";
            this.ShowIcon = false;
            this.Text = "menuInicial";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculadoraComplexaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculadoraSimplesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuLanchoneteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label horizontalDivisor1;
    }
}