﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using TextBox = System.Windows.Forms.TextBox;

namespace tpaProject1
{
    #pragma warning disable IDE1006
    public partial class calculadoraSimples : Form
    {
        double value1 = 0, value2 = 0;
        public calculadoraSimples()
        {
            InitializeComponent();
        }
        private string is_even(double value)
        {
            if (value == 0) { return "Neutro"; }
            if ((value % 2) == 0) { return "Par"; }
            else return "Ímpar";
        }
        private void valueTextBox_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(value1TextBox.Text, out value1);
            double.TryParse(value2TextBox.Text, out value2);
        }
        private void clearButton_Click(object sender, EventArgs e)
        {
            value1TextBox.Text = "";
            value2TextBox.Text = "";
            resultMaskedTextBox.Text = "";
        }
        private void valueTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var TextBox = ((TextBox)sender);

            if (!char.IsNumber(e.KeyChar) && e.KeyChar != '.' && !char.IsNumber(e.KeyChar) && e.KeyChar != '-' && !char.IsNumber(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.' && TextBox.Text.IndexOf(".") > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == '-' && TextBox.SelectionStart != 0)
            {
                e.Handled = true;
            }
            if (e.KeyChar == '-' && TextBox.Text.IndexOf("-") != -1)
            {
                e.Handled = true;
            }
        }
        private void sumButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"{value1 + value2}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void subtractButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"{value1 - value2}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void divideButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"{value1 / value2}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void multiplyButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"{value1 * value2}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void powButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"{(double)Math.Pow(value1, Math.Abs(value2))}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void compareButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (value1 == value2)
                {
                    resultMaskedTextBox.Text = "Os valores são so mesmos";
                }
                else if (value1 > value2)
                {
                    resultMaskedTextBox.Text = $"Maior: {value1TextBox.Text} - Valor 1 | Menor: {value2TextBox.Text}  - Valor 2";
                }
                else
                {
                    resultMaskedTextBox.Text = $"Maior: {value2TextBox.Text} - Valor 1 | Menor: {value1TextBox.Text} - Valor 2";
                }
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
        private void oddevenButton_Click(object sender, EventArgs e)
        {
            try
            {
                resultMaskedTextBox.Text = $"Valor 1: {is_even(value1)} | Valor 2: {is_even(value2)}";
            }
            catch (Exception err)
            {
                resultMaskedTextBox.Text = err.GetType().Name;
            }
        }
    }
}
