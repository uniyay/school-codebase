from os import system, name
from time import sleep

def clear():
    if name == "nt":
        system('cls')
    else:
        system('clear')

def main():
    repeat = True
    end = False
    
    deflist = []
    tmplist = []
    result = []
    numlist = 0
    
    handler = "Matrix Sorter"
    order = "Ascending"
    limit = "None"
    
    while repeat:
        print (handler)
        print ("-------------------")
        print ("[1] New Matrix")
        print ("[2] Confirm")
        print ("[3] Reset\n")
        print ("[0] Exit")
        print ("-------------------")
        if result: print (result)
        usrip = input()
            
        if usrip == "1":
            clear()
            while repeat:
                print ("Split using spaces, type \"end\" to exit")
                print ("-------------------")
                
                tmplist = [str(x) for x in input().split(' ')]
                tmplist = [*set(tmplist)]
                for x in range(len(tmplist)):
                    try:
                        deflist.append(int(tmplist[x]))
                    except:
                        deflist.append(tmplist[x])
                        if tmplist[x] == "end":
                            deflist.pop()
                            repeat = False
                            
                            numlist += 1
                            handler = f"Matrix #{numlist} added"
                            continue
                        deflist.pop()
                    
                    if tmplist[x] == '':
                        deflist.pop()
                
                tmplist = []
                clear()
        
        elif usrip == "2":
            clear()
            while repeat:
                print ("Select an option to change it")
                print ("-------------------")
                print (f"[1] {order}")
                print (f"[2] Limit: {limit}\n")
                print ("[0] Confirm")
                print ("-------------------")
                usrip = input()
                if usrip == "1":
                    clear()
                    if order == "Ascending":
                        order = "Descending"
                    elif order == "Descending":
                        order = "Ascending"
                    clear()
                elif usrip == "2":
                    clear()
                    print ("Type \"None\" to disable the limit")
                    limit = input()
                    
                    if limit == "0":
                        limit = "None"
                    if limit != "None":
                        try:
                            int(limit)
                        except:
                           limit = "None"
                    
                    clear()
                elif usrip == "0":
                    if order == "Descending":
                        ordbool = True
                    else:
                        ordbool = False
                        
                    result = deflist.copy()
                    result.sort(reverse=ordbool)
                        
                    if limit != "None":                        
                        cntlist = len(deflist)
                        
                        while cntlist > int(limit):
                            result.pop()
                            cntlist -= 1
                    
                    repeat = False
                    clear()
                    
                else:
                    clear()
                    print ("Invalid option")
                    print ("-------------------")
        
        elif usrip == "3":
            clear()
            deflist = []
            numlist = 0
            result = []
            handler = "Matrix reseted"
            
        elif usrip == "0":
            clear()
            
            print("Ending...")
            sleep(2)
            
            clear()
            
            repeat = False
            end = True
            
        else:
            clear()
            handler = "Invalid option"
            
        if not end:
            repeat = True

if __name__ == "__main__":
    clear()
    main()
